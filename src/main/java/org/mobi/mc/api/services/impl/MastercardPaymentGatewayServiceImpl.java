package org.mobi.mc.api.services.impl;

import org.joko.core.jsonapi.JSONApiDocument;
import org.mobi.mc.api.MastercardEndPoint;
import org.mobi.mc.api.errors.handlers.RestTemplateErrorHandler;
import org.mobi.mc.modules.gateway.model.CheckGatewayResponse;
import org.mobi.mc.modules.gateway.model.PaymentOptionsInquiryResponse;
import org.mobi.mc.core.http.BaseRestTemplateEngine;
import org.mobi.mc.api.services.IMastercardPaymentGatewayService;
import org.mobi.mc.modules.gateway.model.ResponseObject;
import org.mobi.mc.modules.gateway.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * MasterCard Payments Gateway Service class.
 * Communicates with MasterCard API and delivers gateway-related information.
*/
@Service
public class MastercardPaymentGatewayServiceImpl implements IMastercardPaymentGatewayService
{    
    private final static String CHECK_GATEWAY_STATUS_URI = "https://eu-gateway.mastercard.com/api/rest/version/45/information";   
    private final static String CHECK_GATEWAY_STATUS_URI_ERROR = "https://eu-gateway.mastercard.com/api/rest/version/45/informationERROR";
    private RestTemplate rest;
    private HttpHeaders headers;    
    private static final Logger logger = LoggerFactory.getLogger(MastercardPaymentGatewayServiceImpl.class);   

    @Autowired
    private BaseRestTemplateEngine baseRestTemplateEngine;
     
    
    @Override
    public CheckGatewayResponse gatewayStatus()
    {        
        this.rest = new RestTemplate();
        rest.setErrorHandler(new RestTemplateErrorHandler());
        this.headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "*/*");                   
        
        logger.info("Checking Mastercard Payments Gateway status...");                         
        HttpEntity<CheckGatewayResponse> requestEntity = new HttpEntity<>(null, headers);
        ResponseEntity<CheckGatewayResponse> responseEntity  = rest.exchange(CHECK_GATEWAY_STATUS_URI, HttpMethod.GET, requestEntity, CheckGatewayResponse.class);        
        logger.info("Done, gateway status retrieved: " + responseEntity.getBody().getStatus());          
        return responseEntity.getBody();
    }
  
    @Override
    public CheckGatewayResponse getGatewayInformation()
    {
        return baseRestTemplateEngine.get(MastercardEndPoint.PAYMENTS_GATEWAY_INFORMATION + "error", CheckGatewayResponse.class);
    }

    @Override
    public PaymentOptionsInquiryResponse getPaymentOptionsInquiryResponse(String merchantId)
    {        
        String uri = MastercardEndPoint.PAYMENTS_GATEWAY_API_PATH + "/merchant/" + merchantId + "/paymentOptionsInquiry";
        return baseRestTemplateEngine.get(uri, PaymentOptionsInquiryResponse.class);
    }
    
    public PaymentOptionsInquiryResponse createGatewaySession(String merchantId)
    {        
        String uri = MastercardEndPoint.MERCHANTS + "/" + merchantId + "/session";
        return baseRestTemplateEngine.get(uri, PaymentOptionsInquiryResponse.class);
    }    
    
    public PaymentOptionsInquiryResponse getFundsAuthorization(String merchantId, String transactionId, String orderId)
    {        
        String uri = MastercardEndPoint.MERCHANTS + "/" + merchantId + "/session";
        return baseRestTemplateEngine.get(uri, PaymentOptionsInquiryResponse.class);
    }      

    @Override
    public JSONApiDocument createSession(String merchantId)
    {
        String uri = MastercardEndPoint.MERCHANTS + "/" + merchantId + "/session";
        return new JSONApiDocument(baseRestTemplateEngine.get(uri, PaymentOptionsInquiryResponse.class));
    }

    @Override
    public JSONApiDocument getOrUpdateSession(String merchantId, String sessionId) {
        String uri = MastercardEndPoint.MERCHANTS + "/" + merchantId + "/session/" + sessionId;
        return new JSONApiDocument(baseRestTemplateEngine.get(uri, PaymentOptionsInquiryResponse.class));
    }

    @Override
    public JSONApiDocument searchToken(String merchantId) {
        String uri = MastercardEndPoint.MERCHANTS + "/" + merchantId + "/tokenSearch";
        return new JSONApiDocument(baseRestTemplateEngine.get(uri, PaymentOptionsInquiryResponse.class));
    }

    @Override
    public ResponseObject getToken(String merchantId, String tokenId) {
        String uri = MastercardEndPoint.MERCHANTS + "/" + merchantId + "/token/" + tokenId;
        ResponseObject postData = new ResponseObject();
        baseRestTemplateEngine.put(uri, postData, ResponseObject.class);        
        return baseRestTemplateEngine.get(uri, ResponseObject.class);
    }

    @Override
    public ResponseObject updateToken(String merchantId, String tokenId)
    {
        String uri = MastercardEndPoint.MERCHANTS + "/" + merchantId + "/token/" + tokenId;
        ResponseObject postData = new ResponseObject();
        baseRestTemplateEngine.put(uri, postData, ResponseObject.class);        
        return baseRestTemplateEngine.get(uri, ResponseObject.class);
    }

    @Override
    public void deleteToken(String merchantId, String tokenId)
    {
        String uri = MastercardEndPoint.MERCHANTS + "/" + merchantId + "/token/" + tokenId;
        ResponseObject postData = new ResponseObject();
        baseRestTemplateEngine.delete(uri, postData, ResponseObject.class);        
    }    
    
    @Override
    public Transaction getOrCreateTransaction(String merchantId, String sessionId, String transactionId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
