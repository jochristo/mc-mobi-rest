package org.mobi.mc.modules.gateway.model;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import javax.validation.constraints.NotNull;
import org.joko.core.jsonapi.annotations.JAPIAttribute;
import org.joko.core.jsonapi.annotations.JAPIIdentifier;
import org.joko.core.jsonapi.annotations.JAPIType;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.id.GeneratedValue;
import static org.springframework.data.couchbase.core.mapping.id.GenerationStrategy.UNIQUE;
import org.springframework.format.annotation.NumberFormat;

/**
 *
 * @author ic
 */
@Document
@org.springframework.data.mongodb.core.mapping.Document(collection = "transactions")
@JAPIType(type = "transactions")
public class Transaction {

    @Id @GeneratedValue(strategy = UNIQUE)
    @org.springframework.data.mongodb.core.mapping.Field
    @JAPIIdentifier
    private String id;    
    
    @Field
    @org.springframework.data.mongodb.core.mapping.Field
    private Acquirer acquirer;
    
    @Field
    @org.springframework.data.mongodb.core.mapping.Field
    @NumberFormat(style = NumberFormat.Style.NUMBER)    
    @JAPIAttribute
    private double amount;
        
    @Field
    @org.springframework.data.mongodb.core.mapping.Field
    @NumberFormat(style = NumberFormat.Style.CURRENCY)
    @NotNull(message = "Transaction currency is mandatory")
    @JAPIAttribute
    private String currency;       
    
    @Field
    @org.springframework.data.mongodb.core.mapping.Field
    @NotNull(message = "Transaction type is mandatory")
    @JAPIAttribute
    private TransactionType type;
    

    public Transaction() {
    }       
        
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Acquirer getAcquirer() {
        return acquirer;
    }

    public void setAcquirer(Acquirer acquirer) {
        this.acquirer = acquirer;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    
    
    
}
