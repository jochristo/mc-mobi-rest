package org.mobi.mc.api.controllers;

import org.mobi.mc.api.ApiEndpoints;
import org.mobi.mc.api.Constants;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Transactions RESTful controller class.
 * @author ic
 */
@RestController
@RequestMapping(value = ApiEndpoints.TRANSACTIONS, produces = Constants.APPLICATION_JSON_UTF8_VALUE)
public class TransactionsController
{
    

}
