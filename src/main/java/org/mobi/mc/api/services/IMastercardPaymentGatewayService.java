package org.mobi.mc.api.services;

import org.joko.core.jsonapi.JSONApiDocument;
import org.mobi.mc.modules.gateway.model.CheckGatewayResponse;
import org.mobi.mc.modules.gateway.model.PaymentOptionsInquiryResponse;
import org.mobi.mc.modules.gateway.model.ResponseObject;
import org.mobi.mc.modules.gateway.model.Transaction;

/**
 *
 * @author ic
 */
public interface IMastercardPaymentGatewayService {
    
    public CheckGatewayResponse gatewayStatus();    
    
    public CheckGatewayResponse getGatewayInformation();
    
    public PaymentOptionsInquiryResponse getPaymentOptionsInquiryResponse(String merchantId);
    
    public JSONApiDocument createSession(String merchantId);    
        
    public JSONApiDocument getOrUpdateSession(String merchantId, String sessionId);
    
    public JSONApiDocument searchToken(String merchantId);        
       
    public ResponseObject getToken(String merchantId, String sessionId);        

    public ResponseObject updateToken(String merchantId, String sessionId);        
    
    public void deleteToken(String merchantId, String sessionId);        
    
    public Transaction getOrCreateTransaction (String merchantId, String sessionId, String transactionId);
    
    
    
}
