package org.mobi.mc.modules.gateway.model;

import java.util.Map;

/**
 *
 * @author Admin
 */
public class Merchant
{
    private String gatewayHost;
    private String gatewayUrl;
    private String proxyServer;
    private Integer proxyPort;
    private String proxyUsername;
    private String proxyPassword;
    private String proxyAuthType;
    private String ntDomain;
    private boolean debug;
    private String version;
    private String merchantId;
    private String apiUsername;
    private String password;
    private String trustStorePath;
    private String trustStorePassword;

    public Merchant(Map<String, Object> configuration)
    {
        gatewayHost = (String) configuration.get("gatewayHost");
        proxyServer = (String) configuration.get("proxyServer");
        proxyPort = (Integer) configuration.get("proxyPort");
        proxyUsername = (String) configuration.get("proxyUsername");
        proxyPassword = (String) configuration.get("proxyPassword");
        proxyAuthType = (String) configuration.get("proxyAuthType");
        ntDomain = (String) configuration.get("ntDomain");
        gatewayUrl = (String) configuration.get("gatewayUrl");
        debug = (Boolean) configuration.get("debug");
        version = (String) configuration.get("version");
        merchantId = (String) configuration.get("merchantId");
        password = (String) configuration.get("password");
        apiUsername = (String) configuration.get("apiUsername");        
                
    }
        
    public String getGatewayHost() {
        return gatewayHost;
    }

    public void setGatewayHost(String gatewayHost) {
        this.gatewayHost = gatewayHost;
    }

    public String getGatewayUrl() {
        return gatewayUrl;
    }

    public void setGatewayUrl(String gatewayUrl) {
        this.gatewayUrl = gatewayUrl;
    }

    public String getProxyServer() {
        return proxyServer;
    }

    public void setProxyServer(String proxyServer) {
        this.proxyServer = proxyServer;
    }

    public Integer getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(Integer proxyPort) {
        this.proxyPort = proxyPort;
    }

    public String getProxyUsername() {
        return proxyUsername;
    }

    public void setProxyUsername(String proxyUsername) {
        this.proxyUsername = proxyUsername;
    }

    public String getProxyPassword() {
        return proxyPassword;
    }

    public void setProxyPassword(String proxyPassword) {
        this.proxyPassword = proxyPassword;
    }

    public String getProxyAuthType() {
        return proxyAuthType;
    }

    public void setProxyAuthType(String proxyAuthType) {
        this.proxyAuthType = proxyAuthType;
    }

    public String getNtDomain() {
        return ntDomain;
    }

    public void setNtDomain(String ntDomain) {
        this.ntDomain = ntDomain;
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getApiUsername() {
        return apiUsername;
    }

    public void setApiUsername(String apiUsername) {
        this.apiUsername = apiUsername;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTrustStorePath() {
        return trustStorePath;
    }

    public void setTrustStorePath(String trustStorePath) {
        this.trustStorePath = trustStorePath;
    }

    public String getTrustStorePassword() {
        return trustStorePassword;
    }

    public void setTrustStorePassword(String trustStorePassword) {
        this.trustStorePassword = trustStorePassword;
    }
    
    
}
