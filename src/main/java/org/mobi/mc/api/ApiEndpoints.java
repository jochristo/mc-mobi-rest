/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobi.mc.api;

/**
 *
 * @author Admin
 */
public interface ApiEndpoints
{        
    /**
     * Rest API prefix
     */
    public static final String API_PATH = "/rest/v1";
    
    /**
     * Errors Controller, Spring will redirect all errors to this Endpoint ( DO
     * NOT CHANGE ) access All method - All
     *
     * @see org.atlas.core.handlers.errors.CustomErrorController (error)
     */
    public static final String SPRING_BOOT_ERRORS = "/error";	    
    
    
    // ENDPOINT TO SEE ALL APPLICATION SPECIFIC ERROR CODES WITH DETAILS AND DESCRIPTORS
    public static final String APPLICATION_CODES = "/appcodes";	    

    /**
     * Login endpoint access All method POST - Authentication
     * 
     */
    public static final String AUTHENTICATION_LOGIN = API_PATH + "/auth/login";

    /**
     * logout endpoint access All method GET - Authentication
     * 
     */
    public static final String AUTHENTICATION_LOGOUT = API_PATH + "/auth/logout";

    /**
     * Sign-up access All method POST
     *
     */
    public static final String AUTHENTICATION_SIGNUP = API_PATH + "/auth/signup";    
        
    public static final String ATM_LOCATIONS = API_PATH + "/atms";     
    
    public static final String PAYMENTS = API_PATH + "/payments";         
    
    public static final String TRANSACTIONS = API_PATH + "/transactions";     

    public static final String TRANSACTION_ID = API_PATH + "/transactions/{id}";  
    
    public static final String MERCHANTS = API_PATH + "/merchants";     
    
    // GATEWAY ENDPOINTS
    public static final String GATEWAY = API_PATH + "/gateway";     
    public static final String GATEWAY_INFORMATION = GATEWAY + "/information";     
    public static final String GATEWAY_PAYMENT_OPTIONS_INQUIRY = GATEWAY + "/paymentOptionsInquiry";  
    public static final String GATEWAY_SESSION = GATEWAY + "/session";  
    
    
}
