package org.mobi.mc.modules.gateway.exceptions;

import org.mobi.mc.api.errors.ApiError;
import org.mobi.mc.api.errors.ApiErrorCode;
import org.mobi.mc.core.http.exceptions.AbstractRestTemplateException;
import org.springframework.http.HttpStatus;

/**
 *
 * @author Admin
 */
public class MastercardApiMethodNotAllowedException extends AbstractRestTemplateException
{
    public MastercardApiMethodNotAllowedException(HttpStatus httpStatus, ApiError apiError, String details) {
        super(httpStatus, apiError, details);
    }

    public MastercardApiMethodNotAllowedException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
    }     
}
