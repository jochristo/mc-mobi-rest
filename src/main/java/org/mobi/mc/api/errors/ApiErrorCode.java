package org.mobi.mc.api.errors;

import org.springframework.http.HttpStatus;

/**
 * Custom API error codes
 * @author ic
 */
public enum ApiErrorCode
{

    // API's ERROR COES
    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "500", "INTERNAL_SERVER_ERROR"),
    
    CONSTRAINT_VIOLATION(HttpStatus.UNPROCESSABLE_ENTITY, "1000", "CONSTRAINT_VIOLATION"),

    RESOURCE_NOT_FOUND(HttpStatus.NOT_FOUND, "404", "RESOURCE_NOT_FOUND"),
    
    MISSING_REQUEST_PARAMETER(HttpStatus.BAD_REQUEST, "2001", "MISSING_REQUEST_PARAMETER"),
    
    INVALID_PARAMETER_VALUE(HttpStatus.UNPROCESSABLE_ENTITY, "2002", "INVALID_PARAMETER_VALUE")        ,

    // MASTERCARD API EXCEPTION CODES : 3XXX    
    MASTERCARD_API_SYSTEM_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "3500", "MASTERCARD_API_SYSTEM_ERROR")    ,
    
    MASTERCARD_API_BAD_REQUEST(HttpStatus.BAD_REQUEST, "3400", "MASTERCARD_API_BAD_REQUEST") ,
    
    MASTERCARD_API_UNAUTHORIZED(HttpStatus.UNAUTHORIZED, "3401", "MASTERCARD_API_UNAUTHORIZED") ,    
    
    MASTERCARD_API_FORBIDDEN(HttpStatus.FORBIDDEN, "3403", "MASTERCARD_API_FORBIDDEN") ,
    
    MASTERCARD_API_NOT_FOUND(HttpStatus.NOT_FOUND, "3404", "MASTERCARD_API_NOT_FOUND") ,
    
    MASTERCARD_API_METHOD_NOT_ALLOWED(HttpStatus.METHOD_NOT_ALLOWED, "3405", "MASTERCARD_API_METHOD_NOT_ALLOWED") ,
    
    MASTERCARD_API_UNPROCESSABLE_ENTITY(HttpStatus.UNPROCESSABLE_ENTITY, "3422", "MASTERCARD_API_UNPROCESSABLE_ENTITY") ,
;   
    private final HttpStatus httpStatus;
    private final String appCode;
    private final String title;    
    
    private final int statusCode;
    public int getStatusCode()
    {
        return this.httpStatus.value();
    }
    
    public String getStatusAsString()
    {
        return String.valueOf(this.httpStatus.value());
    }    

    private ApiErrorCode(HttpStatus httpStatus, String appCode, String title)
    {
        this.httpStatus = httpStatus;
        this.appCode = appCode;
        this.title = title;
        this.statusCode = httpStatus.value();
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getAppCode() {
        return appCode;
    }

    public String getTitle() {
        return title;
    }
    

    
    
}
