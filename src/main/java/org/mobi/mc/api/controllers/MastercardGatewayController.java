package org.mobi.mc.api.controllers;

import org.joko.core.jsonapi.JSONApiDocument;
import static org.mobi.mc.api.ApiEndpoints.API_PATH;
import static org.mobi.mc.api.ApiEndpoints.GATEWAY_INFORMATION;
import static org.mobi.mc.api.ApiEndpoints.GATEWAY_PAYMENT_OPTIONS_INQUIRY;
import static org.mobi.mc.api.ApiEndpoints.GATEWAY_SESSION;
import org.mobi.mc.api.Constants;
import org.mobi.mc.modules.gateway.model.CheckGatewayResponse;
import org.mobi.mc.modules.gateway.model.PaymentOptionsInquiryResponse;
import org.mobi.mc.api.services.IMastercardPaymentGatewayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Admin
 */
@RestController
public class MastercardGatewayController
{
    private static final Logger logger = LoggerFactory.getLogger(MastercardGatewayController.class);     
    
    @Autowired
    private IMastercardPaymentGatewayService mastercardPaymentGatewayService;
    

    /**
    * Returns MasterCard Payments Gateway API information/status
    * @return 
    */
    @RequestMapping(value = API_PATH + "/gateway/status", produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET )
    public CheckGatewayResponse checkGatewayResponse()
    {
        CheckGatewayResponse response = mastercardPaymentGatewayService.gatewayStatus();
        return response;
    }
    
    /**
    * Returns MasterCard Payments Gateway API information/status
    * @return 
    */
    @RequestMapping(value = GATEWAY_INFORMATION, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET )
    @ResponseBody
    public CheckGatewayResponse getGatewayInformation()
    {
        return mastercardPaymentGatewayService.getGatewayInformation();
    }    
    
    @RequestMapping(value = GATEWAY_PAYMENT_OPTIONS_INQUIRY, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET )
    @ResponseBody
    public PaymentOptionsInquiryResponse getPaymentOptionInquiry(@RequestParam(name = "merchantId", required = true) String merchantId)
    {
        return mastercardPaymentGatewayService.getPaymentOptionsInquiryResponse(merchantId);
    }     
    
    @RequestMapping(value = GATEWAY_SESSION, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.POST )
    @ResponseBody
    public JSONApiDocument createGatewaySession(@RequestParam(name = "merchantId", required = true) String merchantId)
    {
        return new JSONApiDocument(mastercardPaymentGatewayService.getPaymentOptionsInquiryResponse(merchantId));
    }     
    
}
