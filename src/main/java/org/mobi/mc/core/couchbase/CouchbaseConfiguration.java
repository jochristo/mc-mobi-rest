package org.mobi.mc.core.couchbase;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.env.CouchbaseEnvironment;
import java.util.Arrays;
import java.util.List;
import org.mobi.mc.core.couchbase.repositories.ITransactionRepositoryCb;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;

/**
 * Counchbase server configurer class for spring.
 * @author ic
 */
@Configuration
@EnableCouchbaseRepositories(basePackageClasses = {ITransactionRepositoryCb.class})
public class CouchbaseConfiguration extends AbstractCouchbaseConfiguration
{
    private static final Logger logger = LoggerFactory.getLogger(CouchbaseConfiguration.class); 

    @Value("${couchbase.cluster.bucket}")
    private String bucketName;

    @Value("${couchbase.cluster.password}")
    private String password;

    @Value("${couchbase.cluster.ip}")
    private String ip;      
     
    @Override
    protected List<String> getBootstrapHosts() {
        return Arrays.asList(this.ip);
    }

    @Override
    protected String getBucketName() {
        return this.bucketName;
    }

    @Override
    protected String getBucketPassword() {
        return this.password;
    }

    @Override
    public Bucket couchbaseClient() throws Exception {        
        return super.couchbaseClient(); //To change body of generated methods, choose Tools | Templates.
        
    }

    @Override
    public Cluster couchbaseCluster() throws Exception {
        return super.couchbaseCluster(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected CouchbaseEnvironment getEnvironment() {
        return super.getEnvironment(); //To change body of generated methods, choose Tools | Templates.
    }

    
    

    
}
