package org.mobi.mc.core.utilities;

import java.lang.annotation.Annotation;
import javax.validation.ConstraintViolation;
import javax.validation.metadata.ConstraintDescriptor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author ic
 */
public class ClassTools
{
    public static HttpStatus getHttpStatus(Class<? extends Object> clazz)
    {
        ResponseStatus responseStatusAnnotation =  clazz.getAnnotation(ResponseStatus.class);        
        if(responseStatusAnnotation !=null){
            return responseStatusAnnotation.value();
        }
        
        return HttpStatus.INTERNAL_SERVER_ERROR; // default
    }
    
    public static ResponseStatus getResponseStatus(Class<? extends Object> clazz)
    {
        ResponseStatus responseStatusAnnotation =  clazz.getAnnotation(ResponseStatus.class);        
        if(responseStatusAnnotation !=null){
            return responseStatusAnnotation;
        }
        
        return null;
    }   
    
    public <T extends Annotation> void getConstraintViolationAttributes(ConstraintViolation violation, Class<T> clazz)
    {
        ConstraintDescriptor descriptor = violation.getConstraintDescriptor();
        Class<? extends Annotation> annot = descriptor.getAnnotation().annotationType();
        
       
    }
    
}
