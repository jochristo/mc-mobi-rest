package org.mobi.mc.api.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.joko.core.utilities.Utilities;
import org.mobi.mc.api.annotations.LongitudeAttribute;

/**
 *
 * @author ic
 */
public class LongitudeValidator implements ConstraintValidator<LongitudeAttribute, Object>
{        
    private final String pattern = "^-?((([1]?[0-7][0-9]|[1-9]?[0-9])\\.{1}\\d{1,}$)|[1]?[1-8][0]\\.{1}0{1,}$)";
    
    @Override
    public boolean isValid(Object object, ConstraintValidatorContext cvc)
    {
        if(Utilities.isEmpty(object))
        {
            return true;
        }
        if(String.valueOf(object).equals("0.0")){
            return false;
        }       
        return String.valueOf(object).matches(pattern);
    }      

    @Override
    public void initialize(LongitudeAttribute a) {
        
    }
    
}    

    
    

