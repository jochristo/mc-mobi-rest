/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobi.mc.api.interceptors;

import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author ic
 */
@Component
public class LogInterceptor implements HandlerInterceptor
{
    Logger logger = LoggerFactory.getLogger(LogInterceptor.class);
    @Resource HttpServletRequest httpServletRequest;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception
    {   
        Map<String, String[]> queryParams = request.getParameterMap();
        final StringBuilder logMessage = new StringBuilder("Request started: ")
            .append("[")
            .append(request.getMethod())
            .append("] PATH: [")            
            .append(getHttpRequestUrlNoQueryString(httpServletRequest))            
            .append("] REQUEST PARAMETERS: ")
            .append(queryParams);
        logger.info(logMessage.toString());              
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object o, ModelAndView mav) throws Exception {
        if(o != null)
        {
            logger.info("Method executed: " + o);                 
        }           
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object o, Exception excptn) throws Exception
    {               
        final StringBuilder logMessage = new StringBuilder("Request ended: ")                        
            .append("[")            
            .append(getFullHttpRequestUrl(httpServletRequest))            
            .append("]")
            .append(" with HTTP status ").append(response.getStatus());            
        logger.info(logMessage.toString());                 
    }
    
    private static String getHttpRequestUrlNoQueryString(HttpServletRequest httpServletRequest)
    {
        String scheme = httpServletRequest.getScheme();
        String serverName = httpServletRequest.getServerName();
        int serverPort = httpServletRequest.getServerPort();
        String contextPath = httpServletRequest.getContextPath();
        String servletPath = httpServletRequest.getServletPath();
        //String pathInfo = httpServletRequest.getPathInfo();
        //String queryString = httpServletRequest.getQueryString();
        StringBuilder sb = new StringBuilder(scheme);
        sb.append("://")
                .append(serverName)
                .append(":")
                .append(serverPort)
                .append(contextPath)
                .append(servletPath);
        return sb.toString();
    }    
    
    private static String getFullHttpRequestUrl(HttpServletRequest httpServletRequest)
    {
        String scheme = httpServletRequest.getScheme();
        String serverName = httpServletRequest.getServerName();
        int serverPort = httpServletRequest.getServerPort();
        String contextPath = httpServletRequest.getContextPath();
        String servletPath = httpServletRequest.getServletPath();
        String pathInfo = httpServletRequest.getPathInfo();
        String queryString = httpServletRequest.getQueryString();
        StringBuilder sb = new StringBuilder(scheme);
        sb.append("://")
                .append(serverName)
                .append(":")
                .append(serverPort)
                .append(contextPath)
                .append(servletPath);
        
        if(queryString != null){
            sb.append("?" + queryString);
        }        
        return sb.toString();
    }      
    
}
