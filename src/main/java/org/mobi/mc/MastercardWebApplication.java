
package org.mobi.mc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author ic
 */

@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration
@Configuration
public class MastercardWebApplication
{
    public static void main(String[] args) throws Exception
    {
        SpringApplication.run(MastercardWebApplication.class, args);        
    }          
}
