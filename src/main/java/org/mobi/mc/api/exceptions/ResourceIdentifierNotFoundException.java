package org.mobi.mc.api.exceptions;

import org.mobi.mc.api.errors.ApiError;
import org.mobi.mc.api.errors.ApiErrorCode;
import org.mobi.mc.api.exceptions.base.RestApplicationException;

/**
 *
 * @author ic
 */
public class ResourceIdentifierNotFoundException extends RestApplicationException
{
    public ResourceIdentifierNotFoundException(String details) {
        super(details);
        super.setApiErrorCode(ApiErrorCode.RESOURCE_NOT_FOUND);
    }

    public ResourceIdentifierNotFoundException(ApiError apiError, String details) {
        super(apiError, details);        
    }

    public ResourceIdentifierNotFoundException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
        
    }
    
    
}
