package org.mobi.mc.api.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.joko.core.utilities.Utilities;
import org.mobi.mc.api.annotations.DistanceUnitAttribute;

/**
 *
 * @author ic
 */
public class DistanceUnitValidator implements ConstraintValidator<DistanceUnitAttribute, Object>
{
    private final static String MILE = "MILE";
    private final static String KILOMETER = "KILOMETER";
    
    @Override
    public void initialize(DistanceUnitAttribute a) {        
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext cvc) {
        if(Utilities.isEmpty(object)){
            return true;
        }
        return String.valueOf(object).equalsIgnoreCase(MILE) | String.valueOf(object).equalsIgnoreCase(KILOMETER);
    }
    
}
