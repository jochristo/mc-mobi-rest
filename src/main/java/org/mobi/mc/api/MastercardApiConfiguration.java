package org.mobi.mc.api;

import com.mastercard.api.core.ApiConfig;
import com.mastercard.api.core.security.oauth.OAuthAuthentication;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;

/**
 *
 * @author ic
 */
@Configuration
@PropertySource("mastercard-api.properties")
public class MastercardApiConfiguration
{
    private static final Logger logger = LoggerFactory.getLogger(MastercardApiConfiguration.class);    
    
    @Value("${mastercard.api.consumer.key}")
    private String consumerKey;

    @Value("${mastercard.api.key.alias}")
    private String keyAlias;

    @Value("${mastercard.api.keystore.password}")
    private String keyPassword;

    @Value("${mastercard.api.p12.path}")
    private Resource p12Path;

    @Value("${mastercard.api.sandbox}")
    private boolean sandbox;

    @Value("${mastercard.api.debug}")
    private boolean debug;

    @PostConstruct
    public void setupApiConfiguration() throws Exception {
        logger.debug("setupApiConfiguration");
        //logger.debug(".p12 file path = {}", p12Path.getURI());

        // set API configuration properties
        ApiConfig.setDebug(debug);
        ApiConfig.setSandbox(sandbox);
        ApiConfig.setAuthentication(new OAuthAuthentication(consumerKey, p12Path.getInputStream(), keyAlias, keyPassword));        
    }    
    
}
