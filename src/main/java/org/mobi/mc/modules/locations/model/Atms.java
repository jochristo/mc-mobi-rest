/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobi.mc.modules.locations.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;


/**
 * Atms
 */
public class Atms   {
  @JsonProperty("pageOffset")
  private Integer pageOffset = null;

  @JsonProperty("totalCount")
  private Integer totalCount = null;

  @JsonProperty("atm")
  private List<Atm> atm = new ArrayList<Atm>();

  public Atms pageOffset(Integer pageOffset) {
    this.pageOffset = pageOffset;
    return this;
  }

   /**
   * Zero-based offset where the response will start. The actual start position is this value +1. An offset of 10 starts at item 11. Combined with the PageLength option this allows pagination to be supported through the service requests.
   * @return pageOffset
  **/
  @ApiModelProperty(example = "25", value = "Zero-based offset where the response will start. The actual start position is this value +1. An offset of 10 starts at item 11. Combined with the PageLength option this allows pagination to be supported through the service requests.")
  public Integer getPageOffset() {
    return pageOffset;
  }

  public void setPageOffset(Integer pageOffset) {
    this.pageOffset = pageOffset;
  }

  public Atms totalCount(Integer totalCount) {
    this.totalCount = totalCount;
    return this;
  }

   /**
   * This is the total number of ATMs that match your criteria.
   * @return totalCount
  **/
  @ApiModelProperty(example = "2746", value = "This is the total number of ATMs that match your criteria.")
  public Integer getTotalCount() {
    return totalCount;
  }

  public void setTotalCount(Integer totalCount) {
    this.totalCount = totalCount;
  }

  public Atms atm(List<Atm> atm) {
    this.atm = atm;
    return this;
  }

  public Atms addAtmItem(Atm atmItem) {
    this.atm.add(atmItem);
    return this;
  }

   /**
   * Get atm
   * @return atm
  **/
  @ApiModelProperty(example = "null", value = "")
  public List<Atm> getAtm() {
    return atm;
  }

  public void setAtm(List<Atm> atm) {
    this.atm = atm;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Atms atms = (Atms) o;
    return Objects.equals(this.pageOffset, atms.pageOffset) &&
        Objects.equals(this.totalCount, atms.totalCount) &&
        Objects.equals(this.atm, atms.atm);
  }

  @Override
  public int hashCode() {
    return Objects.hash(pageOffset, totalCount, atm);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Atms {\n");
    
    sb.append("    pageOffset: ").append(toIndentedString(pageOffset)).append("\n");
    sb.append("    totalCount: ").append(toIndentedString(totalCount)).append("\n");
    sb.append("    atm: ").append(toIndentedString(atm)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
