package org.mobi.mc.api.exceptions;

import org.mobi.mc.api.errors.ApiError;
import org.mobi.mc.api.errors.ApiErrorCode;
import org.mobi.mc.api.exceptions.base.RestApplicationException;
import org.springframework.http.HttpStatus;

/**
 *
 * @author ic
 */
public class MissingRequestParameterException extends RestApplicationException
{

    public MissingRequestParameterException(String details) {
        super(details);
        super.setApiErrorCode(ApiErrorCode.MISSING_REQUEST_PARAMETER);
    }

    public MissingRequestParameterException(HttpStatus httpStatus, ApiError apiError, String details) {
        super(httpStatus, apiError, details);
    }

    public MissingRequestParameterException(ApiError apiError, String details) {
        super(apiError, details);
    }       

    public MissingRequestParameterException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
    }
    
    
    
}
