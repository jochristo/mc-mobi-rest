package org.mobi.mc.modules.gateway.model;

/**
 *
 * @author ic
 */
public class PaymentOptionsInquiryResponse
{
    private String correlationId;
    
    private String merchant;
    
    private String result;
    
    private String transactionMode;

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getTransactionMode() {
        return transactionMode;
    }

    public void setTransactionMode(String transactionMode) {
        this.transactionMode = transactionMode;
    }
    
    
}
