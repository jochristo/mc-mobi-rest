package org.mobi.mc.api.services;

import java.util.List;
import org.mobi.mc.modules.gateway.model.Transaction;

/**
 * Defines operations on {@link Transaction} instances stored in NoSQL data stores.
 * @author ic
 */
public interface ITransactionNoSqlService
{
    public List<Transaction> getTransactions();
    
    public Transaction findOne(String id);
    
    public Transaction save(Transaction transaction);
    
    public void delete(Transaction transaction);
    
    public void insert(Transaction transaction);
    
}
