/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobi.mc.api.filters;

import org.springframework.web.filter.CommonsRequestLoggingFilter;

/**
 *
 * @author ic
 */
//@Configuration
public class RequestLoggingFilterConfigurer
{
    //@Bean
    
    public CommonsRequestLoggingFilter requestLoggingFilter()
    {
        CommonsRequestLoggingFilter filter = new CommonsRequestLoggingFilter();
        filter.setIncludeClientInfo(true);
        filter.setIncludeQueryString(true);
        filter.setIncludePayload(true);
        // truncate payloads
        filter.setMaxPayloadLength(1000);
        filter.setIncludeHeaders(false);
        filter.setAfterMessagePrefix("Request ended: ");                
        return filter;
        
    }   
    
}
