package org.mobi.mc.api.controllers;

import com.mastercard.api.core.exception.ApiException;
import com.mastercard.api.core.model.RequestMap;
import com.mastercard.api.locations.ATMLocations;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.joko.core.utilities.Utilities;
import org.mobi.mc.api.ApiEndpoints;
import static org.mobi.mc.api.ApiEndpoints.ATM_LOCATIONS;
import org.mobi.mc.api.Constants;
import org.mobi.mc.api.exceptions.ParameterConstraintViolationException;
import org.mobi.mc.modules.locations.model.Address;
import org.mobi.mc.modules.locations.model.Atm;
import org.mobi.mc.modules.locations.model.AtmQueryRequest;
import org.mobi.mc.modules.locations.model.Atms;
import org.mobi.mc.modules.locations.model.Country;
import org.mobi.mc.modules.locations.model.CountrySubdivision;
import org.mobi.mc.modules.locations.model.Location;
import org.mobi.mc.modules.locations.model.Point;
import org.mobi.mc.api.services.IMastercardPaymentGatewayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Admin
 */
@RestController
public class LocationsController implements Constants
{
    private static final Logger logger = LoggerFactory.getLogger(LocationsController.class);        
    
    //@Autowired 
    //private IRedisService redisService;
    
    @Autowired
    private IMastercardPaymentGatewayService mastercardPaymentGatewayService;
   
    /**
     * Gets all ATM locations with given parameters.
     * @param pageOffset
     * @param pageLength
     * @param postalCode
     * @param country
     * @param longitude
     * @param distanceUnit
     * @param radius
     * @param latitude
     * @return
     * @throws ApiException 
     */
    @RequestMapping(value = ATM_LOCATIONS, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET )
    @Deprecated
    public ATMLocations getAtms(
        @RequestParam(value = "pageOffset", defaultValue = "0", required = false) int pageOffset,
        @RequestParam(value = "pageLength", defaultValue = "20", required = false) int pageLength,                                
        @RequestParam(name = "postalCode", required = false) String postalCode, 
        @RequestParam("country") String country,
        @RequestParam(name = "longitude", required = true, value = "longitude") @NumberFormat long longitude, 
        @RequestParam(name="latitude", required = true, value = "latitude") @NumberFormat long latitude,
        @RequestParam(name = "distanceUnit", required = false, defaultValue = "MILE", value = "distanceUnit") String distanceUnit,
        @RequestParam(name = "radius", required = false, defaultValue = "5", value = "radius") @NumberFormat long radius
    ) throws ApiException
    {
        logger.info("pageOffset (default): " + pageOffset);
        logger.info("pageLength (default): " + pageLength);                
        
        //try
       // {
        
        RequestMap map = new RequestMap();
        map.put("PageOffset", pageOffset);
        map.put("PageLength", pageLength);
        
        if(!Utilities.isEmpty(postalCode)){
            map.put("PostalCode", postalCode);        
            logger.info("Requested postalCode: " + postalCode);
        }
        if(!Utilities.isEmpty(longitude)){
            map.put("Longitude", longitude);        
            logger.info("Requested longitude: " + longitude);
        }
        if(!Utilities.isEmpty(latitude)){
            map.put("Latitude", latitude);        
            logger.info("Requested latitude: " + latitude);
        } 
        if(!Utilities.isEmpty(distanceUnit)){
            map.put("DistanceUnit", distanceUnit);        
            logger.info("Requested distance unit: " + distanceUnit);
        }         
        map.put("Radius", radius);  
        logger.info("Requested radius: " + radius);        
        map.put("Country", country);
        logger.info("Requested country: " + country);
        
        ATMLocations response = ATMLocations.query(map);        
        return response;  
        //}
        //catch (ApiException ex)
        //{
        //    throw new RestApplicationException(HttpStatus.INTERNAL_SERVER_ERROR, ApiError.INTERNAL_SERVER_ERROR, ex.getMessage());
        //}
    }    
    
    /**
     * Gets all ATM locations with supplied request parameter data.
     * @param atmQueryModel
     * @return ATMLocations
     * @throws ApiException 
     */    
    @RequestMapping(value = ApiEndpoints.API_PATH + "/allatms", produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET )
    public ATMLocations getAllAtmLocations(@ModelAttribute AtmQueryRequest atmQueryModel) throws ApiException
    {        
        // bean validation
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();
        Set<ConstraintViolation<AtmQueryRequest>> violations = validator.validate(atmQueryModel);
        if (!violations.isEmpty()) {            
            throw new ParameterConstraintViolationException(violations);
        }                    
           
        // validate model
        atmQueryModel.validate();
        
        RequestMap map = new RequestMap();
        map.put("PageOffset", atmQueryModel.getPageOffset());
        logger.info("Requested PageOffset: " + atmQueryModel.getPageOffset());    
        
        map.put("PageLength", atmQueryModel.getPageLength());            
        logger.info("Requested PageLength: " + atmQueryModel.getPageLength());
        
        if(atmQueryModel.getCountry() != null){
            map.put("Country", atmQueryModel.getCountry());
            logger.info("Requested country: " + atmQueryModel.getCountry());
        }       
        
        if(atmQueryModel.getLongitude() != null){
            Double longitude = Double.parseDouble(atmQueryModel.getLongitude());                        
            map.put("Longitude", longitude);
            logger.info("Requested longitude: " + longitude);
        }
        
        if(atmQueryModel.getLatitude() != null){
            Double latitude = Double.parseDouble(atmQueryModel.getLatitude());
            map.put("Latitude", latitude);
            logger.info("Requested latitude: " + latitude);
        }                
        
        if(atmQueryModel.getRadius() != null){
            map.put("DistanceUnit", atmQueryModel.getDistanceUnit());
            logger.info("Requested distance unit: " + atmQueryModel.getDistanceUnit());
        }
        
        if(atmQueryModel.getRadius() != null){
            map.put("Radius", atmQueryModel.getRadius());
            logger.info("Requested radius: " + atmQueryModel.getRadius());
        }         
        
        // query mastercard service
        ATMLocations response = ATMLocations.query(map);        
        return response;          
    }
    
  
   
    
    
    
    
    
    
    
    //@RequestMapping(value = ATM_LOCATIONS, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET )
    public Atms getAtmsByCountry(@RequestParam(value = "pageOffset", defaultValue = "0", required = false) int pageOffset,
                                @RequestParam(value = "pageLength", defaultValue = "20", required = false) int pageLength,
                                @RequestParam(value = "latitude", required = false) double latitude,
                                @RequestParam(value = "longitude", required = false) double longitude,
                                @RequestParam(value = "distanceUnit", required = false) String distanceUnit,
                                @RequestParam(value = "postalCode", required = false) String postalCode,
                                @RequestParam(value = "country", required = false) String country) throws ApiException {
        RequestMap map = new RequestMap();
        map.put("PageOffset", pageOffset);
        map.put("PageLength", pageLength);
        map.put("Latitude", latitude);
        map.put("Longitude", longitude);
        map.put("DistanceUnit", distanceUnit);
        map.put("PostalCode", postalCode);
        map.put("Country", country);        
        ATMLocations response = ATMLocations.query(map);        
        return processResponse(response);
    }    

    private Atms processResponse(ATMLocations response) {

        Atms atms = new Atms();
        atms.setPageOffset(Integer.valueOf(response.get("Atms.PageOffset").toString()));
        atms.setTotalCount(Integer.valueOf(response.get("Atms.TotalCount").toString()));

        List<Map<String, Object>> list = (List<Map<String, Object>>) response.get("Atms.Atm");
        if (list != null && !list.isEmpty()) {
            List<Atm> atmList = new ArrayList<>(list.size());

            for (Map<String, Object> i : list) {
                Map<String, Object> locationMap = (Map<String, Object>) i.get("Location");
                Map<String, Object> addressMap = (Map<String, Object>) locationMap.get("Address");
                Map<String, Object> subDivisionMap = (Map<String, Object>) addressMap.get("CountrySubdivision");
                Map<String, Object> countryMap = (Map<String, Object>) addressMap.get("Country");
                Map<String, Object> pointMap = (Map<String, Object>) locationMap.get("Point");

                CountrySubdivision subdivision = new CountrySubdivision();
                subdivision.setCode((String) subDivisionMap.get("Code"));
                subdivision.setName((String) subDivisionMap.get("Name"));

                Country country = new Country();
                country.setCode((String) countryMap.get("Code"));
                country.setName((String) countryMap.get("Name"));

                Address address = new Address();
                address.setLine1((String) addressMap.get("Line1"));
                address.setLine2((String) addressMap.get("Line2"));
                address.setCity((String) addressMap.get("City"));
                address.setPostalCode((String) addressMap.get("PostalCode"));
                address.setCountrySubdivision(subdivision);
                address.setCountry(country);

                Point point = new Point();
                point.setLatitude(Double.parseDouble(pointMap.get("Latitude").toString()));
                point.setLongitude(Double.parseDouble(pointMap.get("Longitude").toString()));

                Location location = new Location();
                location.setName((String) locationMap.get("Name"));

                if (locationMap.get("Distance") != null) {
                    Double distance = Double.parseDouble(locationMap.get("Distance").toString());
                    location.setDistance(Math.round(distance * 100) / 100.0);
                }
                location.setDistanceUnit(locationMap.get("DistanceUnit").toString().toLowerCase());
                location.setAddress(address);
                location.setPoint(point);

                Atm atm = new Atm();

                atm.setHandicapAccessible("YES".equals(i.get("HandicapAccessible")));
                atm.setCamera("YES".equals(i.get("Camera")));
                atm.setAvailability((String) i.get("Availability"));
                atm.setAccessFees((String) i.get("AccessFees"));
                atm.setSharedDeposit("YES".equals(i.get("SharedDeposit")));
                atm.setSurchargeFreeAlliance("YES".equals(i.get("SurchargeFreeAlliance")));
                atm.setSupportEmv(Long.valueOf(1).equals(i.get("SupportEMV")));
                atm.setInternationalMaestroAccepted(Long.valueOf(1).equals(i.get("InternationalMaestroAccepted")));
                atm.setLocation(location);

                atmList.add(atm);
            }
            atms.setAtm(atmList);
        }

        return atms;
    }
}
