package org.mobi.mc.api.exceptions.config;

import java.util.Map;
import org.mobi.mc.api.exceptions.base.RestApplicationException;
import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;

@Configuration
public class ErrorsConfig {

    @Bean
    public ErrorAttributes errorAttributes()
    {
        return new DefaultErrorAttributes()
        {
            @Override
            public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes, boolean includeStackTrace)
            {
                Map<String, Object> errorAttributes = super.getErrorAttributes(requestAttributes, includeStackTrace);
                Throwable error = getError(requestAttributes);
                if (error instanceof RestApplicationException) {
                    errorAttributes.put("appCode", ((RestApplicationException) error).getAppCode());
                    errorAttributes.put("details", ((RestApplicationException) error).getDetails());
                }
                return errorAttributes;
            }

        };
    }

}
