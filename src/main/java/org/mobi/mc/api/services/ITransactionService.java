/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobi.mc.api.services;

import java.util.List;
import org.mobi.mc.modules.gateway.model.Transaction;

/**
 *
 * @author Admin
 */
public interface ITransactionService {
 
    public List<Transaction> findALl();
    
    public Transaction findById(String id);
    
    public void save(Transaction tr);
}
