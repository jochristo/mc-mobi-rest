package org.mobi.mc.api.controllers;

import com.mastercard.api.core.exception.ApiException;
import com.mastercard.api.core.model.RequestMap;
import com.mastercard.api.merchantidentifier.MerchantIdentifier;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import static org.mobi.mc.api.ApiEndpoints.MERCHANTS;
import org.mobi.mc.api.exceptions.ParameterConstraintViolationException;
import org.mobi.mc.modules.merchants.model.MerchantIdQueryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Admin
 */
@RestController
public class MerchantsController
{
    private static final Logger logger = LoggerFactory.getLogger(MerchantsController.class);        
    
    //@Autowired 
    //private IRedisService redisService;

    
    @RequestMapping(value = MERCHANTS, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public MerchantIdentifier query(@RequestBody MerchantIdQueryRequest merchantIdQueryRequest) throws ApiException
    {   
        
        // bean validation
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();
        Set<ConstraintViolation<MerchantIdQueryRequest>> violations = validator.validate(merchantIdQueryRequest);
        if (!violations.isEmpty()) {            
            throw new ParameterConstraintViolationException(violations);
        } 
                
        RequestMap map = new RequestMap();
        map.put("MerchantId", merchantIdQueryRequest.getMerchantId());
        map.put("Type", merchantIdQueryRequest.getQueryType());
        
        logger.info("Requested merchantId: " + merchantIdQueryRequest.getMerchantId());
        logger.info("Requested queryType: " + merchantIdQueryRequest.getQueryType());        
        
        MerchantIdentifier merchantIdentifier = MerchantIdentifier.query(map);
        if(merchantIdentifier != null)
        {
            //redisService.hPutIfAbsent("merchants", merchantIdQueryRequest.getMerchantId(), merchantIdentifier,60);
            logger.info("Writing merchantId to redis cache: " + merchantIdQueryRequest.getMerchantId());            
        }
        return merchantIdentifier;
    }    
    
    
    
    
}
