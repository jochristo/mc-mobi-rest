/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobi.mc.modules.locations.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.Objects;

/**
 *
 * @author Admin
 */
public class Error
{
  @JsonProperty("source")
  private String source = null;

  @JsonProperty("reason")
  private String reason = null;

  public Error source(String source) {
    this.source = source;
    return this;
  }

   /**
   * Unique identifier that attempts to define the field in error when available.  If a specific field can't be identified System will be returned.
   * @return source
  **/
  @ApiModelProperty(example = "radius", value = "Unique identifier that attempts to define the field in error when available.  If a specific field can't be identified System will be returned.")
  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public Error reason(String reason) {
    this.reason = reason;
    return this;
  }

   /**
   * Identify the reason for the error.
   * @return reason
  **/
  @ApiModelProperty(example = "null", value = "Identify the reason for the error.")
  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Error error = (Error) o;
    return Objects.equals(this.source, error.source) &&
        Objects.equals(this.reason, error.reason);
  }

  @Override
  public int hashCode() {
    return Objects.hash(source, reason);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Error {\n");
    
    sb.append("    source: ").append(toIndentedString(source)).append("\n");
    sb.append("    reason: ").append(toIndentedString(reason)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }    
    
}
