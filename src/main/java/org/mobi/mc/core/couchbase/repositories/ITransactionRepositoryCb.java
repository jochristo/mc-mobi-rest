package org.mobi.mc.core.couchbase.repositories;

import java.util.List;
import org.mobi.mc.modules.gateway.model.Transaction;
import org.springframework.data.couchbase.core.query.View;
import org.springframework.data.couchbase.repository.CouchbaseRepository;

/**
 *
 * @author ic
 */
public interface ITransactionRepositoryCb extends CouchbaseRepository<Transaction, String>
{   
    
    @View(viewName = "findAll")
    @Override
    public List<Transaction> findAll();
    
    @View
    public List<Transaction> findByType(String type);    
    
    @View
    public List<Transaction> findByCurrency(String currency);   

    @View
    public List<Transaction> findByAcquirer(String acquirerId);        
      
    
}
