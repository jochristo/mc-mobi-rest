/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobi.mc.modules.gateway.exceptions;

import org.mobi.mc.api.errors.ApiError;
import org.mobi.mc.api.errors.ApiErrorCode;
import org.mobi.mc.core.http.exceptions.AbstractRestTemplateException;
import org.springframework.http.HttpStatus;

/**
 *
 * @author Admin
 */
public class MastercardApiBadRequestException extends AbstractRestTemplateException
{    
    public MastercardApiBadRequestException(HttpStatus httpStatus, ApiError apiError, String details) {
        super(httpStatus, apiError, details);
    }

    public MastercardApiBadRequestException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
    }    
        
}
