/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobi.mc.modules.gateway.model;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Admin
 */
public class Order
{    
    private double amount;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME, pattern = "YYYY-MM-DDThh:mm:ss.SSSZ")
    private Date creationTime;
        
    private String currency;
    
    private String id;
    
    private String gatewayCode;
    
    private double totalAuthorizedAmount;
    
    private double totalCapturedAmount;
    
    private double totalRefundedAmount;

    
    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGatewayCode() {
        return gatewayCode;
    }

    public void setGatewayCode(String gatewayCode) {
        this.gatewayCode = gatewayCode;
    }

    public double getTotalAuthorizedAmount() {
        return totalAuthorizedAmount;
    }

    public void setTotalAuthorizedAmount(double totalAuthorizedAmount) {
        this.totalAuthorizedAmount = totalAuthorizedAmount;
    }

    public double getTotalCapturedAmount() {
        return totalCapturedAmount;
    }

    public void setTotalCapturedAmount(double totalCapturedAmount) {
        this.totalCapturedAmount = totalCapturedAmount;
    }

    public double getTotalRefundedAmount() {
        return totalRefundedAmount;
    }

    public void setTotalRefundedAmount(double totalRefundedAmount) {
        this.totalRefundedAmount = totalRefundedAmount;
    }
    
    
    
}
