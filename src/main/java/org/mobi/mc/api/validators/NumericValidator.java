package org.mobi.mc.api.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.joko.core.utilities.Utilities;
import org.mobi.mc.api.annotations.NumericAttribute;
import static org.mobi.mc.api.annotations.NumericAttribute.AttributeType.LATITUDE;
import static org.mobi.mc.api.annotations.NumericAttribute.AttributeType.LONGITUDE;

/**
 * Constraint validity class for profile properties: latitude, longitude, mobile no, and generic numbers.
 * @author ic
 */
public class NumericValidator implements ConstraintValidator<NumericAttribute, Object>
{
    //regular expression matching patterns
    private final String LatitudePattern = "^-?(([1-8]?[0-9]\\.{1}\\d{1,})|([1-8]?[0-9])$|(90\\.{1}0{1,})|(90)$)"; //"^-?([1-8]?[0-9]\\.{1}\\d{1,}$|90\\.{1}0{1,}$)";        
    private final String LongitudePattern = "^-?((([1]?[0-7][0-9]|[1-9]?[0-9])(\\.{1}\\d{1,})?$)|[1]?[1-8][0](\\.{1}0{1,})?$)"; //"^-?((([1]?[0-7][0-9]|[1-9]?[0-9])\\.{1}\\d{1,}$)|[1]?[1-8][0]\\.{1}0{1,}$)";       
    private final String generic = "^[0]|\\d+$";
    private final String mobileNoPattern = "^[+][0-9]+\\d+$";
    private NumericAttribute numericAttribute;   
   
    //pattern to use
    private String pattern = null;        
        
    @Override
    public void initialize(NumericAttribute a) {
        
        NumericAttribute.AttributeType attributeType = a.pattern();
        numericAttribute = a;
        switch (attributeType)
        {
            case LONGITUDE: 
                pattern = LongitudePattern;
                break;            
            case LATITUDE: 
                this.pattern = LatitudePattern;
                break;
            case GENERAL_NUMBER: 
                pattern = generic;
                break;                
        }   
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext cvc) 
    {           
        if(Utilities.isEmpty(object))
        {
            return true;
        }
        
        if(numericAttribute.pattern() == LONGITUDE || numericAttribute.pattern() == LATITUDE){
            if(String.valueOf(object).equals("0.0")){
                return false;
            }         
        }
        return String.valueOf(object).matches(pattern);
    }
        
}