package org.mobi.mc.api.errors.handlers;

import java.io.IOException;
import org.mobi.mc.api.errors.ApiErrorCode;
import org.mobi.mc.modules.gateway.exceptions.MastercardApiBadRequestException;
import org.mobi.mc.modules.gateway.exceptions.MastercardApiForbiddenException;
import org.mobi.mc.modules.gateway.exceptions.MastercardApiInternalServerErrorException;
import org.mobi.mc.modules.gateway.exceptions.MastercardApiMethodNotAllowedException;
import org.mobi.mc.modules.gateway.exceptions.MastercardApiNotFoundException;
import org.mobi.mc.modules.gateway.exceptions.MastercardApiUnauthorizedException;
import org.mobi.mc.modules.gateway.exceptions.MastercardApiUnprocessableEntityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

/**
 * RestTemplate engine response handler.
 * Handles HTTP REST calls to services: e.g. MasterCard gateway services.
 * @author ic
 */
public class RestTemplateErrorHandler implements ResponseErrorHandler
{
    private final Logger logger = LoggerFactory.getLogger(RestTemplateErrorHandler.class);
    
    @Override
    public boolean hasError(ClientHttpResponse httpResponse) throws IOException
    {        
        return httpResponse.getStatusCode() != HttpStatus.OK;
    }

    @Override
    public void handleError(ClientHttpResponse httpResponse) throws IOException
    {   
        handleResponse(httpResponse);
    }
    
    /**
     * Handles response based on status code.
     * Throws error-specific custom exceptions with specified application code.
     * @param httpResponse
     * @throws IOException 
     */
    protected void handleResponse(ClientHttpResponse httpResponse) throws IOException
    {        
        int status = httpResponse.getRawStatusCode();

        switch (status) {

            case 400: throw new MastercardApiBadRequestException(ApiErrorCode.MASTERCARD_API_BAD_REQUEST, httpResponse.getStatusText());
            
            case 401: throw new MastercardApiUnauthorizedException(ApiErrorCode.MASTERCARD_API_UNAUTHORIZED, httpResponse.getStatusText());            

            case 403: throw new MastercardApiForbiddenException(ApiErrorCode.MASTERCARD_API_FORBIDDEN, httpResponse.getStatusText());
            
            case 404: throw new MastercardApiNotFoundException(ApiErrorCode.MASTERCARD_API_NOT_FOUND, httpResponse.getStatusText());                            

            case 405: throw new MastercardApiMethodNotAllowedException(ApiErrorCode.MASTERCARD_API_METHOD_NOT_ALLOWED, httpResponse.getStatusText());                                        
                                    
            case 422: throw new MastercardApiUnprocessableEntityException(ApiErrorCode.MASTERCARD_API_UNPROCESSABLE_ENTITY, httpResponse.getStatusText());                
                
            case 500: throw new MastercardApiInternalServerErrorException(ApiErrorCode.MASTERCARD_API_SYSTEM_ERROR, httpResponse.getStatusText());

            default:
                logger.error("MastercardApiInternalServerErrorException occured, status: " + status + " ( " + httpResponse.getStatusText() + " )");
                throw new MastercardApiInternalServerErrorException(ApiErrorCode.MASTERCARD_API_SYSTEM_ERROR, httpResponse.getStatusText());               
        }        


    }
    
}
