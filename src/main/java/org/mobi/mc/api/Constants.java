package org.mobi.mc.api;

import org.springframework.http.MediaType;

/**
 *
 * @author ic
 */
public interface Constants {

    
    public final static String APPLICATION_JSON_UTF8_VALUE = MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8";
    
    public final static MediaType APPLICATION_JSON_UTF8 = MediaType.valueOf(APPLICATION_JSON_UTF8_VALUE);     
    
    // define which MC payments gateway services API version to use
    public final static String MPGS_API_VERSION = "45";
    
    /**
     * Error codes for application-specific errors.
     */
    public enum ErrorCode
    {                
        NOT_EMPTY_ATTRIBUTE(1001),
        NICKNAME_ATTRIBUTE(1002),
        EMAIL_ATTRIBUTE(1003),
        JSON_DATE_ATTRIBUTE(1004),        
        INVALID_JSON_FORMAT(1005),
        DUPLICATE_EMAIL(1006),
        DUPLICATE_NICKNAME(1007),
        MEMCACHED_TIMEOUT(1008),
        PASSWORD_MISMATCH(1009),
        INCORRECT_PASSWORD(1010),
        INVALID_LONGITUDE_ERROR_CODE(1011),
        INVALID_LATITUDE_ERROR_CODE(1012),
        INVALID_SCORE_ERROR_CODE(1013),
        INVALID_LEVEL_ERROR_CODE(1014),
        INVALID_MOBILE_NO_ERROR_CODE(1015),                
        INVALID_NUMBER_GENERAL(1016),
        DUPLICATE_ANSWERS(1017),
        INVALID_ANSWER_ATTRIBUTE_ERROR_CODE(1018),
        INVALID_IMAGE_FORMAT(1020),
        
        MASTERCARD_API_ERROR_CODE(3000),        
        ;        
        
        private ErrorCode(int value) { this.value = value; }
        private final int value;        
        public int getCode(){
            return this.value;
        }
        public String getCodeAsString() {
            return String.valueOf(this.value);
        }                
    }    
    
    public interface ErrorCodeMessage
    {
        //invalid attribute messages
        public static final String INVALID_LONGITUDE_MESSAGE = "Longitude must be between -180 and 180 degrees inclusive and not 0.0";    
        public static final String INVALID_LATITUDE_MESSAGE = "Latitude must be between -90 and 90 degrees inclusive and not 0.0";                        
        public static final String INVALID_RADIUS_MESSAGE = "Radius must be an unsigned integer value";         
    }
    
    public interface ErrorCodeTitle
    {
        //invalid attribute titles
        public static final String INVALID_LONGITUDE_TITLE  = "Invalid longitude value";
        public static final String INVALID_LATITUDE_TITLE = "Invalid latitude value";        
        public static final String INVALID_RADIUS_TITLE  = "Invalid radius value";        
    }
    
}
