/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobi.mc.modules.gateway.model;

/**
 *
 * @author Admin
 */
public enum TransactionType
{
    AUTHORIZATION(0, "AUTHORIZATION"),
    
    AUTHORIZATION_UPDATE(1, "AUTHORIZATION_UPDATE"),
    
    CAPTURE(2, "CAPTURE"),
    
    PAYMENT(3, "PAYMENT"),
    
    REFUND(4, "REFUND"),
    
    REFUND_REQUEST(5, "REFUND_REQUEST"),
    
    VERIFICATION(6, "VERIFICATION"),
    
    VOID_AUTHORIZATION(7, "VOID_AUTHORIZATION"),
    
    VOID_CAPTURE(8, "VOID_CAPTURE"),
    
    VOID_PAYMENT(9, "VOID_PAYMENT"),
    
    VOID_REFUND(10, "VOID_REFUND");
    
    private final int value;
    private final String label;
    private TransactionType(int x, String label)
    {
        this.value = x;
        this.label = label;
    }
    
    public String getType()
    {
        return this.label;
    }
    
    public int getValue()
    {
        return this.value;
    }
    
}
