package org.mobi.mc.core.mongodb.repositories;

import java.util.List;
import org.mobi.mc.modules.gateway.model.Transaction;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author ic
 */

public interface ITransactionRepositoryMongo extends MongoRepository<Transaction, String>
{        
    public Transaction findById(String id);

    @Override
    public List<Transaction> findAll();
    
    
}
