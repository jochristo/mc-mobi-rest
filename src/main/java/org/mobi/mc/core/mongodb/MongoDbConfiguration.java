package org.mobi.mc.core.mongodb;

import com.mongodb.Mongo;
import org.mobi.mc.core.mongodb.repositories.ITransactionRepositoryMongo;
import org.mobi.mc.core.mongodb.services.TransactionMongoService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoClientFactoryBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 *
 * @author Admin
 */
@Configuration
@EnableMongoRepositories(basePackageClasses = {ITransactionRepositoryMongo.class})
public class MongoDbConfiguration
{
    @Value("${mongodb.database.name}")
    private String databaseName;    
        
    @Bean
    public MongoTemplate mongoTemplate(Mongo mongo) throws Exception {                
        return new MongoTemplate(mongoFactoryBean().getObject(), databaseName);
    }    
    
    @Bean
    public MongoClientFactoryBean mongoFactoryBean() {
    return new MongoClientFactoryBean();
    }        
    
    @Bean
    public TransactionMongoService transactionRepositoryMongo(MongoTemplate mongo) {
       return new TransactionMongoService();
    } 
    
    
}
