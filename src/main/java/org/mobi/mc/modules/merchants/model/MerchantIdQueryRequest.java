package org.mobi.mc.modules.merchants.model;

import javax.validation.constraints.NotNull;

/**
 *
 * @author ic
 */
public class MerchantIdQueryRequest {
    
    private String merchantId;
    private String queryType;

    @NotNull(message = "merchantId query parameter is required")
    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    @NotNull(message = "queryType query parameter is required")
    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }
    
    
}
