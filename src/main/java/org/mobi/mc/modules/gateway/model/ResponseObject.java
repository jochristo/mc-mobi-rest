/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobi.mc.modules.gateway.model;

/**
 *
 * @author Admin
 */
public class ResponseObject {
    
    private String acquirerCode;   
    
    private String gatewayCode;
    
    private String acquirerMessage;
    
    private String debugInformation;

    public String getAcquirerCode() {
        return acquirerCode;
    }

    public void setAcquirerCode(String acquirerCode) {
        this.acquirerCode = acquirerCode;
    }

    public String getGatewayCode() {
        return gatewayCode;
    }

    public void setGatewayCode(String gatewayCode) {
        this.gatewayCode = gatewayCode;
    }

    public String getAcquirerMessage() {
        return acquirerMessage;
    }

    public void setAcquirerMessage(String acquirerMessage) {
        this.acquirerMessage = acquirerMessage;
    }

    public String getDebugInformation() {
        return debugInformation;
    }

    public void setDebugInformation(String debugInformation) {
        this.debugInformation = debugInformation;
    }
    
    
}
