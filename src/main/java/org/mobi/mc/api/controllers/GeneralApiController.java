
package org.mobi.mc.api.controllers;

import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import org.joko.core.jsonapi.JSONApiDocument;
import static org.mobi.mc.api.ApiEndpoints.API_PATH;
import static org.mobi.mc.api.ApiEndpoints.APPLICATION_CODES;
import org.mobi.mc.api.Constants;
import org.mobi.mc.api.errors.Utils;
import org.mobi.mc.modules.gateway.model.Acquirer;
import org.mobi.mc.modules.gateway.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.mobi.mc.api.services.ITransactionNoSqlService;
import org.mobi.mc.modules.gateway.model.TransactionType;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author ic
 */
@RestController
@RequestMapping(value = API_PATH +  "/internal")
public class GeneralApiController
{
    private static final Logger logger = LoggerFactory.getLogger(GeneralApiController.class);     
    private static final String DUMMY_MONGO_ID = "5a3a41a96f97c421f8f0d122";    
    
    @Autowired    
    @Qualifier("couchbaseService")  
    private ITransactionNoSqlService couchbaseService;
    
    @Autowired
    @Qualifier("mongodbService")  
    private ITransactionNoSqlService mongodbService;    
       
    @RequestMapping(value = APPLICATION_CODES, produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET )
    @ResponseBody
    public JSONApiDocument getAppCodes()
    {
        List codes = Utils.applicationCodes();
        logger.info("Acquiring application-specific error codes", codes);
        return new JSONApiDocument(codes);
    }
    
    @RequestMapping(value = "/collection", produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET )
    @ResponseBody
    public JSONApiDocument createMongoDbCollectionEntry()
    {               
        logger.info("Counting MongoDB collections: ", mongodbService.getTransactions().size());
        Transaction transaction = new Transaction();
        transaction.setId(UUID.randomUUID().toString() + "_mongodb");
        transaction.setAcquirer(new Acquirer());
        transaction.setAmount(1000);
        transaction.setCurrency("EUR");
        transaction.setType(TransactionType.PAYMENT);
        logger.info("Saving transaction information to MongoDB...");        
        mongodbService.insert(transaction);
        logger.info("Done.");
        logger.info("Loading transaction information from MongoDB...");
        Transaction tr = mongodbService.findOne(transaction.getId());        
        return new JSONApiDocument(tr);
    }    
    
    @RequestMapping(value = "/bucket", produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.GET )
    @ResponseBody
    public JSONApiDocument createCbBucketEntryGET()
    {        
        Transaction transaction = new Transaction();
        //transaction.setId(UUID.randomUUID().toString() + "_couchbase");        
        transaction.setAcquirer(new Acquirer());
        transaction.setAmount(10000);
        transaction.setCurrency("EUR");
         transaction.setType(TransactionType.REFUND);
        logger.info("Saving transaction document to Couchbase...");        
        couchbaseService.save(transaction);
        logger.info("Done.");
        logger.info("Loading transaction document from Couchbase...");
        Transaction tr = couchbaseService.findOne(transaction.getId());        
        return new JSONApiDocument(tr);
    }      
  
    @RequestMapping(value = "/transactions", produces = Constants.APPLICATION_JSON_UTF8_VALUE, method=RequestMethod.POST )    
    @ResponseBody
    public JSONApiDocument createCbTransaction(@RequestBody @Valid Transaction transaction)
    {                
        logger.info("Saving transaction document to Couchbase...");        
        couchbaseService.save(transaction);
        logger.info("Done.");
        logger.info("Loading transaction document from Couchbase...");
        Transaction tr = couchbaseService.findOne(transaction.getId());        
        return new JSONApiDocument(tr);
    }     
    
}
