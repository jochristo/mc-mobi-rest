package org.mobi.mc.modules.gateway.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Admin
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CheckGatewayResponse {
    
    @JsonProperty("gatewayVersion")
    private String gatewayVersion;
    
    @JsonProperty("status")
    private String status;

    public String getGatewayVersion() {
        return gatewayVersion;
    }

    public void setGatewayVersion(String gatewayVersion) {
        this.gatewayVersion = gatewayVersion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
}
