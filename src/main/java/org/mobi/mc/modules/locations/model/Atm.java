/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobi.mc.modules.locations.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;


/**
 * Atm
 */
public class Atm   {
  @JsonProperty("location")
  private Location location = null;

  @JsonProperty("handicapAccessible")
  private Boolean handicapAccessible = null;

  @JsonProperty("camera")
  private Boolean camera = null;

  @JsonProperty("availability")
  private String availability = null;

  @JsonProperty("accessFees")
  private String accessFees = null;

  @JsonProperty("sharedDeposit")
  private Boolean sharedDeposit = null;

  @JsonProperty("surchargeFreeAlliance")
  private Boolean surchargeFreeAlliance = null;

  @JsonProperty("supportEmv")
  private Boolean supportEmv = null;

  @JsonProperty("internationalMaestroAccepted")
  private Boolean internationalMaestroAccepted = null;

  public Atm location(Location location) {
    this.location = location;
    return this;
  }

   /**
   * Get location
   * @return location
  **/
  @ApiModelProperty(example = "null", value = "")
  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public Atm handicapAccessible(Boolean handicapAccessible) {
    this.handicapAccessible = handicapAccessible;
    return this;
  }

   /**
   * This value indicates whether or not the ATM is accessible by wheelchair.
   * @return handicapAccessible
  **/
  @ApiModelProperty(example = "true", value = "This value indicates whether or not the ATM is accessible by wheelchair.")
  public Boolean getHandicapAccessible() {
    return handicapAccessible;
  }

  public void setHandicapAccessible(Boolean handicapAccessible) {
    this.handicapAccessible = handicapAccessible;
  }

  public Atm camera(Boolean camera) {
    this.camera = camera;
    return this;
  }

   /**
   * This value indicates whether or not a security camera is present or near ATM.
   * @return camera
  **/
  @ApiModelProperty(example = "true", value = "This value indicates whether or not a security camera is present or near ATM.")
  public Boolean getCamera() {
    return camera;
  }

  public void setCamera(Boolean camera) {
    this.camera = camera;
  }

  public Atm availability(String availability) {
    this.availability = availability;
    return this;
  }

   /**
   * This value indicates the availability hours of the ATM. Options are UNKNOWN, ALWAYS_AVAILABLE, BUSINESS_HOURS, IRREGULAR_HOURS.
   * @return availability
  **/
  @ApiModelProperty(example = "ALWAYS_AVAILABLE", value = "This value indicates the availability hours of the ATM. Options are UNKNOWN, ALWAYS_AVAILABLE, BUSINESS_HOURS, IRREGULAR_HOURS.")
  public String getAvailability() {
    return availability;
  }

  public void setAvailability(String availability) {
    this.availability = availability;
  }

  public Atm accessFees(String accessFees) {
    this.accessFees = accessFees;
    return this;
  }

   /**
   * This value indicates under what conditions access fees are charged. Options are UNKNOWN, DOMESTIC, INTERNATIONAL, DOMESTIC_AND_INTERNATIONAL, NO_FEE.
   * @return accessFees
  **/
  @ApiModelProperty(example = "DOMESTIC", value = "This value indicates under what conditions access fees are charged. Options are UNKNOWN, DOMESTIC, INTERNATIONAL, DOMESTIC_AND_INTERNATIONAL, NO_FEE.")
  public String getAccessFees() {
    return accessFees;
  }

  public void setAccessFees(String accessFees) {
    this.accessFees = accessFees;
  }

  public Atm sharedDeposit(Boolean sharedDeposit) {
    this.sharedDeposit = sharedDeposit;
    return this;
  }

   /**
   * This value indicates whether or not the ATM participates in the MasterCard Shared Deposit network.
   * @return sharedDeposit
  **/
  @ApiModelProperty(example = "true", value = "This value indicates whether or not the ATM participates in the MasterCard Shared Deposit network.")
  public Boolean getSharedDeposit() {
    return sharedDeposit;
  }

  public void setSharedDeposit(Boolean sharedDeposit) {
    this.sharedDeposit = sharedDeposit;
  }

  public Atm surchargeFreeAlliance(Boolean surchargeFreeAlliance) {
    this.surchargeFreeAlliance = surchargeFreeAlliance;
    return this;
  }

   /**
   * This value indicates whether or not the ATM participates in the MasterCard Shared (only) Surcharge Free Alliance network.
   * @return surchargeFreeAlliance
  **/
  @ApiModelProperty(example = "true", value = "This value indicates whether or not the ATM participates in the MasterCard Shared (only) Surcharge Free Alliance network.")
  public Boolean getSurchargeFreeAlliance() {
    return surchargeFreeAlliance;
  }

  public void setSurchargeFreeAlliance(Boolean surchargeFreeAlliance) {
    this.surchargeFreeAlliance = surchargeFreeAlliance;
  }

  public Atm supportEmv(Boolean supportEmv) {
    this.supportEmv = supportEmv;
    return this;
  }

   /**
   * This indicates whether the ATM has the ability to read chip cards or not.
   * @return supportEmv
  **/
  @ApiModelProperty(example = "true", value = "This indicates whether the ATM has the ability to read chip cards or not.")
  public Boolean getSupportEmv() {
    return supportEmv;
  }

  public void setSupportEmv(Boolean supportEmv) {
    this.supportEmv = supportEmv;
  }

  public Atm internationalMaestroAccepted(Boolean internationalMaestroAccepted) {
    this.internationalMaestroAccepted = internationalMaestroAccepted;
    return this;
  }

   /**
   * This field will provide ATM Terminals which can still process Maestro transactions but are not yet EMV chip reader enabled. Information available only for USA and Argentina till October 2014.
   * @return internationalMaestroAccepted
  **/
  @ApiModelProperty(example = "true", value = "This field will provide ATM Terminals which can still process Maestro transactions but are not yet EMV chip reader enabled. Information available only for USA and Argentina till October 2014.")
  public Boolean getInternationalMaestroAccepted() {
    return internationalMaestroAccepted;
  }

  public void setInternationalMaestroAccepted(Boolean internationalMaestroAccepted) {
    this.internationalMaestroAccepted = internationalMaestroAccepted;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Atm atm = (Atm) o;
    return Objects.equals(this.location, atm.location) &&
        Objects.equals(this.handicapAccessible, atm.handicapAccessible) &&
        Objects.equals(this.camera, atm.camera) &&
        Objects.equals(this.availability, atm.availability) &&
        Objects.equals(this.accessFees, atm.accessFees) &&
        Objects.equals(this.sharedDeposit, atm.sharedDeposit) &&
        Objects.equals(this.surchargeFreeAlliance, atm.surchargeFreeAlliance) &&
        Objects.equals(this.supportEmv, atm.supportEmv) &&
        Objects.equals(this.internationalMaestroAccepted, atm.internationalMaestroAccepted);
  }

  @Override
  public int hashCode() {
    return Objects.hash(location, handicapAccessible, camera, availability, accessFees, sharedDeposit, surchargeFreeAlliance, supportEmv, internationalMaestroAccepted);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Atm {\n");
    
    sb.append("    location: ").append(toIndentedString(location)).append("\n");
    sb.append("    handicapAccessible: ").append(toIndentedString(handicapAccessible)).append("\n");
    sb.append("    camera: ").append(toIndentedString(camera)).append("\n");
    sb.append("    availability: ").append(toIndentedString(availability)).append("\n");
    sb.append("    accessFees: ").append(toIndentedString(accessFees)).append("\n");
    sb.append("    sharedDeposit: ").append(toIndentedString(sharedDeposit)).append("\n");
    sb.append("    surchargeFreeAlliance: ").append(toIndentedString(surchargeFreeAlliance)).append("\n");
    sb.append("    supportEmv: ").append(toIndentedString(supportEmv)).append("\n");
    sb.append("    internationalMaestroAccepted: ").append(toIndentedString(internationalMaestroAccepted)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
