/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mobi.mc.api.errors;

import java.util.ArrayList;
import java.util.List;
import static org.mobi.mc.api.errors.ApiErrorCode.CONSTRAINT_VIOLATION;
import static org.mobi.mc.api.errors.ApiErrorCode.INTERNAL_SERVER_ERROR;
import static org.mobi.mc.api.errors.ApiErrorCode.INVALID_PARAMETER_VALUE;
import static org.mobi.mc.api.errors.ApiErrorCode.MASTERCARD_API_BAD_REQUEST;
import static org.mobi.mc.api.errors.ApiErrorCode.MASTERCARD_API_FORBIDDEN;
import static org.mobi.mc.api.errors.ApiErrorCode.MASTERCARD_API_METHOD_NOT_ALLOWED;
import static org.mobi.mc.api.errors.ApiErrorCode.MASTERCARD_API_NOT_FOUND;
import static org.mobi.mc.api.errors.ApiErrorCode.MASTERCARD_API_SYSTEM_ERROR;
import static org.mobi.mc.api.errors.ApiErrorCode.MASTERCARD_API_UNAUTHORIZED;
import static org.mobi.mc.api.errors.ApiErrorCode.MASTERCARD_API_UNPROCESSABLE_ENTITY;
import static org.mobi.mc.api.errors.ApiErrorCode.MISSING_REQUEST_PARAMETER;
import static org.mobi.mc.api.errors.ApiErrorCode.RESOURCE_NOT_FOUND;

/**
 *
 * @author Admin
 */
public class Utils {
    
    /**
     * Gets a list of all application-specific error codes.
     * @return 
     */
    public static List<ApplicationCode> applicationCodes()
    {
        List<ApplicationCode> codes = new ArrayList();
        ApplicationCode code = new ApplicationCode();
        code.setStatus(INTERNAL_SERVER_ERROR.getStatusAsString());
        code.setCode(INTERNAL_SERVER_ERROR.getAppCode());
        code.setDetails(INTERNAL_SERVER_ERROR.getTitle());
        codes.add(code);
        
        code = new ApplicationCode();
        code.setStatus(CONSTRAINT_VIOLATION.getStatusAsString());
        code.setCode(CONSTRAINT_VIOLATION.getAppCode());
        code.setDetails(CONSTRAINT_VIOLATION.getTitle());
        codes.add(code);
        
        code = new ApplicationCode();
        code.setStatus(RESOURCE_NOT_FOUND.getStatusAsString());
        code.setCode(RESOURCE_NOT_FOUND.getAppCode());
        code.setDetails(RESOURCE_NOT_FOUND.getTitle());
        codes.add(code);        
        
        code = new ApplicationCode();
        code.setStatus(MISSING_REQUEST_PARAMETER.getStatusAsString());
        code.setCode(MISSING_REQUEST_PARAMETER.getAppCode());
        code.setDetails(MISSING_REQUEST_PARAMETER.getTitle());
        codes.add(code); 
        
        code = new ApplicationCode();
        code.setStatus(INVALID_PARAMETER_VALUE.getStatusAsString());
        code.setCode(INVALID_PARAMETER_VALUE.getAppCode());
        code.setDetails(INVALID_PARAMETER_VALUE.getTitle());
        codes.add(code);
        
        code = new ApplicationCode();
        code.setStatus(MASTERCARD_API_SYSTEM_ERROR.getStatusAsString());
        code.setCode(MASTERCARD_API_SYSTEM_ERROR.getAppCode());
        code.setDetails(MASTERCARD_API_SYSTEM_ERROR.getTitle());
        codes.add(code);
        
        code = new ApplicationCode();
        code.setStatus(MASTERCARD_API_BAD_REQUEST.getStatusAsString());
        code.setCode(MASTERCARD_API_BAD_REQUEST.getAppCode());
        code.setDetails(MASTERCARD_API_BAD_REQUEST.getTitle());
        codes.add(code);
        
        code = new ApplicationCode();
        code.setStatus(MASTERCARD_API_FORBIDDEN.getStatusAsString());
        code.setCode(MASTERCARD_API_FORBIDDEN.getAppCode());
        code.setDetails(MASTERCARD_API_FORBIDDEN.getTitle());
        codes.add(code);
        
        code = new ApplicationCode();
        code.setStatus(MASTERCARD_API_METHOD_NOT_ALLOWED.getStatusAsString());
        code.setCode(MASTERCARD_API_METHOD_NOT_ALLOWED.getAppCode());
        code.setDetails(MASTERCARD_API_METHOD_NOT_ALLOWED.getTitle());
        codes.add(code);
        
        code = new ApplicationCode();
        code.setStatus(MASTERCARD_API_NOT_FOUND.getStatusAsString());
        code.setCode(MASTERCARD_API_NOT_FOUND.getAppCode());
        code.setDetails(MASTERCARD_API_NOT_FOUND.getTitle());
        codes.add(code);
        
        code = new ApplicationCode();
        code.setStatus(MASTERCARD_API_UNAUTHORIZED.getStatusAsString());
        code.setCode(MASTERCARD_API_UNAUTHORIZED.getAppCode());
        code.setDetails(MASTERCARD_API_UNAUTHORIZED.getTitle());
        codes.add(code);
        
        code = new ApplicationCode();
        code.setStatus(MASTERCARD_API_UNPROCESSABLE_ENTITY.getStatusAsString());
        code.setCode(MASTERCARD_API_UNPROCESSABLE_ENTITY.getAppCode());
        code.setDetails(MASTERCARD_API_UNPROCESSABLE_ENTITY.getTitle());
        codes.add(code);
        
        return codes;        
    }
}
