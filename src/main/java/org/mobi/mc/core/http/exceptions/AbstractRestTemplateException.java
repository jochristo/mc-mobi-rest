package org.mobi.mc.core.http.exceptions;

import org.mobi.mc.api.errors.ApiError;
import org.mobi.mc.api.errors.ApiErrorCode;
import org.mobi.mc.api.exceptions.base.RestApplicationException;
import org.springframework.http.HttpStatus;

/**
 *
 * @author ic
 */
public abstract class AbstractRestTemplateException extends RestApplicationException
{
    public AbstractRestTemplateException(String details) {
        super(details);
        super.setApiErrorCode(ApiErrorCode.RESOURCE_NOT_FOUND);
    }

    public AbstractRestTemplateException(ApiError apiError, String details) {
        super(apiError, details);        
    }

    public AbstractRestTemplateException(ApiErrorCode apiErrorCode, String details) {
        super(apiErrorCode, details);
        
    }
    
    public AbstractRestTemplateException(HttpStatus httpStatus, ApiError apiError, String details) {
        super(details);
        this.appCode = apiError.getAppCode();
        this.details = details;
        this.title = apiError.getDetails();
        this.httpStatus = httpStatus;
    }    
}
