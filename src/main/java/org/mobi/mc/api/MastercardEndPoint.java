package org.mobi.mc.api;

import static org.mobi.mc.api.Constants.MPGS_API_VERSION;

/**
 *
 * @author ic
 */
public interface MastercardEndPoint
{            
    public static final String PAYMENTS_GATEWAY_API_PATH = "https://eu-gateway.mastercard.com/api/rest/version/" + MPGS_API_VERSION;
    
    public static final String PAYMENTS_GATEWAY_INFORMATION = PAYMENTS_GATEWAY_API_PATH + "/information";
    
    public static final String MERCHANTS= PAYMENTS_GATEWAY_API_PATH + "/merchant";
    
    public static final String CREATE_SESSION = PAYMENTS_GATEWAY_API_PATH + "/merchant/{merchantId}/session"; // [POST]
    
    public static final String GET_SESSION = PAYMENTS_GATEWAY_API_PATH + "/merchant/{merchantId}/session/{sessionId}"; // [GET,PUT]
    
    public static final String PAYMENT_OPTIONS_INQUIRY = PAYMENTS_GATEWAY_API_PATH + "/merchant/{merchantId}/paymentOptionsInquiry"; // [GET]
    
    public static final String TOKEN = PAYMENTS_GATEWAY_API_PATH + "/merchant/{merchantId}/token/{tokenid}"; // [GET,PUT,DELETE]
    
    public static final String SYSTEM_GENERATED_TOKEN = PAYMENTS_GATEWAY_API_PATH + "/merchant/{merchantId}/token"; // [POST]
    
    public static final String TRANSACTION_ORDER = PAYMENTS_GATEWAY_API_PATH + "/merchant/{merchantId}/order/{orderid}"; // [GET]
    
    public static final String TRANSACTION = TRANSACTION_ORDER + "/transaction/{transactionid}"; // [GET,PUT]           
        
}
