package org.mobi.mc.api.advisors;

import com.mastercard.api.core.exception.ApiCommunicationException;
import com.mastercard.api.core.exception.ApiException;
import com.mastercard.api.core.exception.AuthenticationException;
import com.mastercard.api.core.exception.InvalidRequestException;
import com.mastercard.api.core.exception.MessageSignerException;
import com.mastercard.api.core.exception.NotAllowedException;
import com.mastercard.api.core.exception.ObjectNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.joko.core.jsonapi.JSONApiDocument;
import org.joko.core.jsonapi.JSONApiErrors;
import org.mobi.mc.api.errors.ApiError;
import org.mobi.mc.api.errors.ApiErrorCode;
import org.mobi.mc.modules.gateway.exceptions.MastercardApiBadRequestException;
import org.mobi.mc.modules.gateway.exceptions.MastercardApiForbiddenException;
import org.mobi.mc.modules.gateway.exceptions.MastercardApiInternalServerErrorException;
import org.mobi.mc.modules.gateway.exceptions.MastercardApiMethodNotAllowedException;
import org.mobi.mc.modules.gateway.exceptions.MastercardApiNotFoundException;
import org.mobi.mc.modules.gateway.exceptions.MastercardApiUnauthorizedException;
import org.mobi.mc.modules.gateway.exceptions.MastercardApiUnprocessableEntityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Exception handling advisor for master-card core API.
 * @author ic
 */
@ControllerAdvice(annotations = {RestController.class})
public class MastercardApiExceptionAdvice
{
    
    private static final Logger logger = LoggerFactory.getLogger(MastercardApiExceptionAdvice.class);
    
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)    
    @ExceptionHandler({ApiException.class, ApiCommunicationException.class})
    @ResponseBody
    public JSONApiDocument handleApiException(ApiException ex) {
        logger.error(ex.getClass().getTypeName() + " occured: ", ex);
        JSONApiErrors error = new JSONApiErrors();
        error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
        error.setCode(ApiErrorCode.MASTERCARD_API_SYSTEM_ERROR.getAppCode());
        error.setTitle(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        error.setDetail(ex.getMessage());                
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();        
               
        // handle errors
        if(ex.getErrors().isEmpty() == false)
        {
            List errors = ex.getErrors();
            for(Object obj : errors){
                Map map = (Map) obj;
                Object reasonCode = map.get("ReasonCode");
                Object source = map.get("Source");
                if(reasonCode != null)
                {
                    error.setTitle(String.valueOf(reasonCode));
                }
                if(source != null)
                {
                    error.setDetail("Mastercard API error source: " + String.valueOf(source));
                }                
            }            
        }        

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;        
    }   
    
    @ResponseStatus(HttpStatus.UNAUTHORIZED) 
    @ExceptionHandler({AuthenticationException.class,MessageSignerException.class})
    public JSONApiDocument handleAuthenticationException(AuthenticationException ex) {
        logger.error(ex.getClass().getTypeName() + " occured: ", ex);
        JSONApiErrors error = new JSONApiErrors();
        error.setStatus(HttpStatus.UNAUTHORIZED.toString());
        error.setCode(ApiErrorCode.MASTERCARD_API_UNAUTHORIZED.getAppCode());
        error.setTitle(HttpStatus.UNAUTHORIZED.getReasonPhrase());
        error.setDetail(ex.getMessage());         
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();        
               
        // handle errors
        if(ex.getErrors().isEmpty() == false)
        {
            List errors = ex.getErrors();
            for(Object obj : errors){
                Map map = (Map) obj;
                Object reasonCode = map.get("ReasonCode");
                Object source = map.get("Source");
                if(reasonCode != null)
                {
                    error.setTitle(String.valueOf(reasonCode));
                }
                if(source != null)
                {
                    error.setDetail("Mastercard API error source: " + String.valueOf(source));
                }                
            }            
        }        

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;   
    }    
    
    @ResponseStatus(HttpStatus.BAD_REQUEST) 
    @ExceptionHandler(InvalidRequestException.class)
    public JSONApiDocument handleInvalidRequestException(InvalidRequestException ex) {
        logger.error(ex.getClass().getTypeName() + " occured: ", ex);
        JSONApiErrors error = new JSONApiErrors();
        error.setStatus(HttpStatus.BAD_REQUEST.toString());
        error.setCode(ApiErrorCode.MASTERCARD_API_BAD_REQUEST.getAppCode());
        error.setTitle(HttpStatus.BAD_REQUEST.getReasonPhrase());
        error.setDetail(ex.getMessage());        
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();        
               
        // handle errors
        if(ex.getErrors().isEmpty() == false)
        {
            List errors = ex.getErrors();
            for(Object obj : errors){
                Map map = (Map) obj;
                Object reasonCode = map.get("ReasonCode");
                Object source = map.get("Source");
                if(reasonCode != null)
                {
                    error.setTitle(String.valueOf(reasonCode));
                }
                if(source != null)
                {
                    error.setDetail("Mastercard API error source: " + String.valueOf(source));
                }                
            }            
        }        

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;   
    }     
    
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED) 
    @ExceptionHandler(NotAllowedException.class)
    public JSONApiDocument handleNotAllowedException(NotAllowedException ex) {
        logger.error(ex.getClass().getTypeName() + " occured: ", ex);
        JSONApiErrors error = new JSONApiErrors();
        error.setStatus(HttpStatus.METHOD_NOT_ALLOWED.toString());
        error.setCode(ApiErrorCode.MASTERCARD_API_METHOD_NOT_ALLOWED.getAppCode());
        error.setTitle(HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase());
        error.setDetail(ex.getMessage());        
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();        
               
        // handle errors
        if(ex.getErrors().isEmpty() == false)
        {
            List errors = ex.getErrors();
            for(Object obj : errors){
                Map map = (Map) obj;
                Object reasonCode = map.get("ReasonCode");
                Object source = map.get("Source");
                if(reasonCode != null)
                {
                    error.setTitle(String.valueOf(reasonCode));
                }
                if(source != null)
                {
                    error.setDetail("Mastercard API error source: " + String.valueOf(source));
                }                
            }            
        }        

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;   
    } 

    @ResponseStatus(HttpStatus.NOT_FOUND) 
    @ExceptionHandler(ObjectNotFoundException.class)
    public JSONApiDocument handleObjectNotFoundException(ObjectNotFoundException ex) {        
        logger.error(ex.getClass().getTypeName() + " occured: ", ex);
        JSONApiErrors error = new JSONApiErrors();
        error.setStatus(HttpStatus.NOT_FOUND.toString());
        error.setCode(ApiErrorCode.MASTERCARD_API_NOT_FOUND.getAppCode());
        error.setTitle(HttpStatus.NOT_FOUND.getReasonPhrase());
        error.setDetail(ex.getMessage());        
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();        
               
        // handle errors
        if(ex.getErrors().isEmpty() == false)
        {
            List errors = ex.getErrors();
            for(Object obj : errors){
                Map map = (Map) obj;
                Object reasonCode = map.get("ReasonCode");
                Object source = map.get("Source");
                if(reasonCode != null)
                {
                    error.setTitle(String.valueOf(reasonCode));
                }
                if(source != null)
                {
                    error.setDetail("Mastercard API error source: " + String.valueOf(source));
                }                
            }            
        }        

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;   
    } 
    
    // CUSTOM MASTERCARD API EXCEPTION HANDLERS
    
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({MastercardApiInternalServerErrorException.class})
    @ResponseBody
    public JSONApiDocument handleMastercardApiInternalServerErrorException(MastercardApiInternalServerErrorException ex)
    {        
        String exceptionType = ex.getClass().getSimpleName();
        logger.error(exceptionType + " occured: ", ex);
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(ex.getHttpStatus().toString());
        error.setCode(ex.getAppCode());
        error.setTitle(ex.getTitle());
        error.setDetail(ex.getDetails());

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }      
    
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler({MastercardApiBadRequestException.class})
    public JSONApiDocument handleMastercardApiBadRequestException(MastercardApiBadRequestException ex)
    {        
        String exceptionType = ex.getClass().getSimpleName();
        logger.error(exceptionType + " occured: ", ex);
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(ex.getHttpStatus().toString());
        error.setCode(ex.getAppCode());
        error.setTitle(ex.getTitle());
        error.setDetail(ex.getDetails());

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }    
    
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    @ExceptionHandler({MastercardApiForbiddenException.class})
    public JSONApiDocument handleMastercardApiForbiddenException(MastercardApiForbiddenException ex)
    {        
        String exceptionType = ex.getClass().getSimpleName();
        logger.error(exceptionType + " occured: ", ex);
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(ex.getHttpStatus().toString());
        error.setCode(ex.getAppCode());
        error.setTitle(ex.getTitle());
        error.setDetail(ex.getDetails());

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }     
    
    @ResponseStatus(value = HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler({MastercardApiMethodNotAllowedException.class})
    public JSONApiDocument handleMastercardApiMethodNotAllowedException(MastercardApiMethodNotAllowedException ex)
    {        
        String exceptionType = ex.getClass().getSimpleName();
        logger.error(exceptionType + " occured: ", ex);
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(ex.getHttpStatus().toString());
        error.setCode(ex.getAppCode());
        error.setTitle(ex.getTitle());
        error.setDetail(ex.getDetails());

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }     
    
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler({MastercardApiNotFoundException.class})
    public JSONApiDocument handleMastercardApiNotFoundException(MastercardApiNotFoundException ex)
    {        
        String exceptionType = ex.getClass().getSimpleName();
        logger.error(exceptionType + " occured: ", ex);
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(ex.getHttpStatus().toString());
        error.setCode(ex.getAppCode());
        error.setTitle(ex.getTitle());
        error.setDetail(ex.getDetails());

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }    
    
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    @ExceptionHandler({MastercardApiUnauthorizedException.class})
    public JSONApiDocument handleMastercardApiUnauthorizedException(MastercardApiUnauthorizedException ex)
    {        
        String exceptionType = ex.getClass().getSimpleName();
        logger.error(exceptionType + " occured: ", ex);
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(ex.getHttpStatus().toString());
        error.setCode(ex.getAppCode());
        error.setTitle(ex.getTitle());
        error.setDetail(ex.getDetails());

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }      
    
    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler({MastercardApiUnprocessableEntityException.class})
    public JSONApiDocument handleMastercardApiUnprocessableEntityException(MastercardApiUnprocessableEntityException ex)
    {        
        String exceptionType = ex.getClass().getSimpleName();
        logger.error(exceptionType + " occured: ", ex);
        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(ex.getHttpStatus().toString());
        error.setCode(ex.getAppCode());
        error.setTitle(ex.getTitle());
        error.setDetail(ex.getDetails());

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }       
    
    @Deprecated
    @ResponseBody
    public JSONApiDocument handleThrowable(Throwable ex)
    {        
        ApiException ae = (ApiException) ex;
        logger.error(ex.getClass().getTypeName() + " occured: ", ex);

        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();
        JSONApiErrors error = new JSONApiErrors();

        error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
        error.setCode(ApiError.ILLEGAL_ARGUMENT_TYPE.getAppCode());
        error.setTitle(HttpStatus.BAD_REQUEST.getReasonPhrase());
        //error.setDetail(ae.getMessage());
        
        // handle errors
        if(ae.getErrors().isEmpty() == false)
        {
            List errors = ae.getErrors();
            for(Object obj : errors){
                Map map = (Map) obj;
                Object reasonCode = map.get("ReasonCode");
                if(reasonCode != null)
                {
                    error.setDetail(String.valueOf(reasonCode));
                }
            }
        }        

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }   
    
    @Deprecated
    @ResponseBody
    public JSONApiDocument createErrorFromThrowable(Throwable ex, JSONApiErrors error)
    {        
        ApiException ae = (ApiException) ex;
        logger.error(ex.getClass().getTypeName() + " occured: ", ex);

        JSONApiDocument errorDocument = new JSONApiDocument();
        ArrayList<JSONApiErrors> errorsList = new ArrayList<JSONApiErrors>();        
               
        // handle errors
        if(ae.getErrors().isEmpty() == false)
        {
            List errors = ae.getErrors();
            for(Object obj : errors){
                Map map = (Map) obj;
                Object reasonCode = map.get("ReasonCode");
                if(reasonCode != null)
                {
                    error.setDetail(String.valueOf(reasonCode));
                }
            }
        }        

        errorsList.add(error);
        errorDocument.setErrors(errorsList);
        return errorDocument;
    }     
}
