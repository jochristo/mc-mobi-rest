package org.mobi.mc.api.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.joko.core.utilities.Utilities;
import org.mobi.mc.api.annotations.RadiusAttribute;

/**
 *
 * @author ic
 */
public class RadiusValidator implements ConstraintValidator<RadiusAttribute, Object>
{

    private final String pattern = "\\d{0,}";
    
    @Override
    public boolean isValid(Object object, ConstraintValidatorContext cvc)
    {
        if(Utilities.isEmpty(object))
        {
            return true;
        }       
        return String.valueOf(object).matches(pattern);
    }      

    @Override
    public void initialize(RadiusAttribute a) {
        
    }
    
}
