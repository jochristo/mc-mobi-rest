package org.mobi.mc.modules.locations.model;

import static org.mobi.mc.api.Constants.ErrorCode.INVALID_LATITUDE_ERROR_CODE;
import static org.mobi.mc.api.Constants.ErrorCode.INVALID_LONGITUDE_ERROR_CODE;
import static org.mobi.mc.api.Constants.ErrorCodeMessage.INVALID_LATITUDE_MESSAGE;
import static org.mobi.mc.api.Constants.ErrorCodeMessage.INVALID_LONGITUDE_MESSAGE;
import static org.mobi.mc.api.Constants.ErrorCodeMessage.INVALID_RADIUS_MESSAGE;
import org.mobi.mc.api.annotations.DistanceUnitAttribute;
import org.mobi.mc.api.annotations.NumericAttribute;
import static org.mobi.mc.api.annotations.NumericAttribute.AttributeType.GENERAL_NUMBER;
import static org.mobi.mc.api.annotations.NumericAttribute.AttributeType.LATITUDE;
import static org.mobi.mc.api.annotations.NumericAttribute.AttributeType.LONGITUDE;
import org.mobi.mc.api.errors.ApiErrorCode;
import org.mobi.mc.api.exceptions.MissingRequestParameterException;

/**
 * Atm query model-attribute class
 * @author ic
 */
public class AtmQueryRequest
{
    
    private int PageOffset = 0;

    private int PageLength = 25;
    
    private String Country;
        
    private String Longitude;
   
    private String Latitude;    

    private String DistanceUnit = "MILE";

    private String Radius; 

    public int getPageOffset() {
        return PageOffset;
    }

    public void setPageOffset(int PageOffset) {
        this.PageOffset = PageOffset;
    }

    public int getPageLength() {
        return PageLength;
    }

    public void setPageLength(int PageLength) {
        this.PageLength = PageLength;
    }

    //@NotNull(message = "country parameter is required")    
    public String getCountry() {
        return Country;
    }

    public void setCountry(String Country) {
        this.Country = Country;
    }

    @NumericAttribute(pattern = LONGITUDE, errorCode = INVALID_LONGITUDE_ERROR_CODE, message = INVALID_LONGITUDE_MESSAGE)
    public String getLongitude() {
        return Longitude;
    }
    
    public void setLongitude(String Longitude) {
        this.Longitude = Longitude;
    }

    @NumericAttribute(pattern = LATITUDE, errorCode = INVALID_LATITUDE_ERROR_CODE, message = INVALID_LATITUDE_MESSAGE)
    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String Latitude) {
        this.Latitude = Latitude;
    }

    @DistanceUnitAttribute
    public String getDistanceUnit() {
        return DistanceUnit;
    }

    public void setDistanceUnit(String DistanceUnit) {
        this.DistanceUnit = DistanceUnit;
    }
    
    @NumericAttribute(pattern = GENERAL_NUMBER, message = INVALID_RADIUS_MESSAGE)
    public String getRadius() {
        return Radius;
    }

    public void setRadius(String Radius) {
        this.Radius = Radius;
    }
    
    public void validate()
    {
        if(this.getLatitude() != null && this.getLongitude() == null){
            //throw new RestApplicationException(HttpStatus.BAD_REQUEST, ApiError.MISSING_REQUEST_PARAMETER, "Longitude is required");
            throw new MissingRequestParameterException(ApiErrorCode.MISSING_REQUEST_PARAMETER, "Longitude parameter is compulsory");
        }
        else if(this.getLatitude() == null && this.getLongitude() != null){
            //throw new RestApplicationException(HttpStatus.BAD_REQUEST, ApiError.MISSING_REQUEST_PARAMETER, "Latitude is required");
            throw new MissingRequestParameterException(ApiErrorCode.MISSING_REQUEST_PARAMETER, "Latitude parameter is compulsory");                    
        }    
        else if(this.getLatitude() == null && this.getLongitude() == null)
        {            
            throw new MissingRequestParameterException(ApiErrorCode.MISSING_REQUEST_PARAMETER, "Longitude/Latitude parameters are compulsory");            
        }
    }
    
}
