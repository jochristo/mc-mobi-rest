package org.mobi.mc.core.redis;

/**
 * Defines the caching operations on implementing redis service class.
 * Value and hash methods.
 * @author ic
 * @param <T> The type of cached object
 */
public interface IRedisService<T extends Object>
{
    public void setExpiry(int exp);
                
    public void add(String key, T value);
    public void add(String key, T value, long expiration);
    public T added(String key, T value);    
    public void update(String key, T value);
    public T updated(String key, T value);  
    boolean hasKey(String key);
    String randomKey();
    long getExpire(String key);
    
    //hash-ops
    public void hAdd(String type, String key, T value);
    public void hAdd(String type, String key, T value, long expiration);
    public T hAdded(String type, String key, T value);   
    boolean hExistsHashKey(String type, String key);
    void hPutIfAbsent(String type, String key, T value);  
    public void hPutIfAbsent(String type, String key, T value, long expirationInSeconds);
    public T hGet(String h, String hashKey);
}
