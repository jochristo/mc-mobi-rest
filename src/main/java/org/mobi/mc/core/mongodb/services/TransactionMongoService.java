package org.mobi.mc.core.mongodb.services;

import org.mobi.mc.api.services.ITransactionNoSqlService;
import java.util.List;
import org.mobi.mc.core.mongodb.repositories.ITransactionRepositoryMongo;
import org.mobi.mc.modules.gateway.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ic
 */
@Service("mongodbService")
public class TransactionMongoService implements ITransactionNoSqlService
{
    @Autowired
    private ITransactionRepositoryMongo mongodbRepository;            
    
    @Override
    public List<Transaction> getTransactions()
    {
        return this.mongodbRepository.findAll();        
    }
    
    @Override
    public Transaction findOne(String id)
    {
        return this.mongodbRepository.findById(id);        
    }

    @Override
    public Transaction save(Transaction transaction) {
        return mongodbRepository.save(transaction);
    }

    @Override
    public void delete(Transaction transaction) {
        mongodbRepository.delete(transaction);
    }

    @Override
    public void insert(Transaction transaction) {
        mongodbRepository.insert(transaction);
    }
    
    
    
}
