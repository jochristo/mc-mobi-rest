package org.mobi.mc.core.couchbase.services;

import java.util.ArrayList;
import java.util.List;
import org.mobi.mc.modules.gateway.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.mobi.mc.api.services.ITransactionNoSqlService;
import org.mobi.mc.core.couchbase.repositories.ITransactionRepositoryCb;

/**
 *
 * @author Admin
 */
@Service("couchbaseService")
public class TransactionCbService  implements ITransactionNoSqlService
{
    @Autowired
    private ITransactionRepositoryCb couchbaseRepository;     

    @Override
    public List<Transaction> getTransactions() {
        List list = new ArrayList();
        couchbaseRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public Transaction findOne(String id) {
       return couchbaseRepository.findOne(id);
    }

    @Override
    public Transaction save(Transaction transaction) {               
        return couchbaseRepository.save(transaction);
    }

    @Override
    public void delete(Transaction transaction) {
        couchbaseRepository.delete(transaction);
    }

    @Override
    public void insert(Transaction transaction) {
        couchbaseRepository.getCouchbaseOperations().insert(transaction);
    }
    
    
    
}
