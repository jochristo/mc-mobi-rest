package org.mobi.mc.api.annotations;

import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import org.mobi.mc.api.validators.RadiusValidator;

/**
 *
 * @author ic
 */
@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = RadiusValidator.class)
@Documented
public @interface RadiusAttribute {
    String message() default "Radius must be in integer format";
    String title() default "Invalid radius format";
    String code() default "1011";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};      
}
