package org.mobi.mc.modules.gateway.model.converters;

import org.mobi.mc.modules.gateway.model.TransactionType;
import org.springframework.core.convert.converter.Converter;

/**
 *
 * @author ic
 */
public class TransactionTypeConverter implements Converter<TransactionType, String> {


    @Override
    public String convert(TransactionType in) {
        return in.getType();
    }


    
}
